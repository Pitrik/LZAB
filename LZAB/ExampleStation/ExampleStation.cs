﻿using System.Collections.Generic;
using System.Drawing;
using Core.Contracts;
using Core.Models;
using System.Linq;

namespace ExampleStation
{
    /// <summary>
    /// Reprezentace závěrové tabulky ukázkové oblasti
    /// </summary>
    public class ExampleStation : IStation
    {
        private readonly IList<Infrastructure> _il;

        public ExampleStation()
        {
            _il = new List<Infrastructure>
            {
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(1, 1),
                    Name = "LT",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(1, 9),
                    LocationEnd = new Point(4, 9)
                    }
                },
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(1, 2),
                    Name = "1aK",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(6, 9),
                    LocationEnd = new Point(9, 9)
                    }
                },
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(2, 1),
                    Name = "1K",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(13, 9),
                    LocationEnd = new Point(28, 9)
                    }
                },
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(2, 2),
                    Name = "2K",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(13, 8),
                    LocationEnd = new Point(28, 8)
                    }
                },
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(3, 2),
                    Name = "1bK",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(32, 9),
                    LocationEnd = new Point(35, 9)
                    }
                },
                new TrackModel
                {
                    InputInterfaces = new InputOutputInterface(3, 1),
                    Name = "ST",
                    State = TrackStates.UNKNOWN,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(37, 9),
                    LocationEnd = new Point(40, 9)
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(1, 1),
                    Name = "L",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(5, 9),
                    LocationEnd = new Point(5, 9),
                    Orientation = Orientation.LR
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(3, 1),
                    Name = "S",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(36, 9),
                    LocationEnd = new Point(36, 9),
                    Orientation = Orientation.RL
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(2, 3),
                    Name = "L1",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(29, 9),
                    LocationEnd = new Point(29, 9),
                    Orientation = Orientation.LR
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(2, 4),
                    Name = "L2",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(29, 8),
                    LocationEnd = new Point(29, 8),
                    Orientation = Orientation.LR
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(2, 1),
                    Name = "S1",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(12, 9),
                    LocationEnd = new Point(12, 9),
                    Orientation = Orientation.RL
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(2, 2),
                    Name = "S2",
                    State = Signals.STUJ,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(12, 8),
                    LocationEnd = new Point(12, 8),
                    Orientation = Orientation.RL
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(3, 3),
                    Name = "STA",
                    State = Signals.VS_V,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(40, 10),
                    LocationEnd = new Point(40, 10),
                    Orientation = Orientation.RL
                    }
                },
                new SemaphoreModel
                {
                    InputInterfaces = new InputOutputInterface(1, 3),
                    Name = "LTA",
                    State = Signals.VS_V,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(1, 10),
                    LocationEnd = new Point(1, 10),
                    Orientation = Orientation.LR
                    }
                },
                new SwitchModel
                {
                    InputInterfaces = new InputOutputInterface(1, 2),
                    Name = "1",
                    State = SwitchStates.S,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(10, 9),
                    LocationEnd = new Point(11, 9),
                    Orientation = Orientation.LR
                    }
                },
                new SwitchModel
                {
                    InputInterfaces = new InputOutputInterface(3, 2),
                    Name = "4",
                    State = SwitchStates.R,
                    ViewModel = new InfrastructureViewModel
                    {
                    LocationStart = new Point(30, 9),
                    LocationEnd = new Point(31, 9),
                    Orientation = Orientation.RL
                    }
                },
            };
        }

        public IList<Infrastructure> GetInfrastrucureList()
        {
            return _il;
        }

        public string GetStationName()
        {
            return "Example Station";
        }

        public IList<RouteRecord> GetStationTable()
        {
            return new List<RouteRecord>
            {
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L2") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "2K") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LT") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "1") as SwitchModel, SwitchStates.L)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1aK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L1") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "1K") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LT") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "1") as SwitchModel, SwitchStates.S)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1aK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S2") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "2K") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "ST") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "4") as SwitchModel, SwitchStates.R)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1bK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },

                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S1") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "1K") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "ST") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "4") as SwitchModel, SwitchStates.S)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1bK") as TrackModel
                    },
                    Type = RouteTypes.VC | RouteTypes.PC
                },

                //

                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "STA") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "ST") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L2") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "2K") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "4") as SwitchModel, SwitchStates.R)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1bK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "STA") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "ST") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "L1") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "1K") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "4") as SwitchModel, SwitchStates.S)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1bK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LTA") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LT") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S2") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "2K") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "1") as SwitchModel, SwitchStates.L)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1aK") as TrackModel
                    },
                    Type = RouteTypes.VC
                },
                new RouteRecord
                {
                    EndSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LTA") as SemaphoreModel,
                    EndTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "LT") as TrackModel,
                    SpeedLimit = null,
                    StartSemaphore = GetInfrastrucureList().FirstOrDefault(x => x.Name == "S1") as SemaphoreModel,
                    StartTrack = GetInfrastrucureList().FirstOrDefault(x => x.Name == "1K") as TrackModel,
                    SwitchesAndPositions = new List<Switch>
                    {
                        new Switch(GetInfrastrucureList().FirstOrDefault(x => x.Name == "1") as SwitchModel, SwitchStates.S)
                    },
                    ViaTracks = new List<TrackModel>
                    {
                        GetInfrastrucureList().FirstOrDefault(x => x.Name == "1aK") as TrackModel
                    },
                    Type = RouteTypes.VC
                }
            };
        }
    }
}