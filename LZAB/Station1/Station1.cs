﻿using System.Collections.Generic;
using Core.Contracts;
using Core.Models;

namespace Station1
{
    public class Station1 : IStation
    {
        private readonly string _stationName;

        public Station1()
        {
            _stationName = "Stanice 1";
        }

        public IList<Infrastructure> GetInfrastrucureList()
        {
            var infrastructureList = new List<Infrastructure>
            {
                /*new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd = 
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },
                new Infrastructure
                {
                    ModulId = ,
                    RegId = ,
                    Name = "",
                    State = ,
                    Type = ,
                    LocationStart = ,
                    LocationEnd =
                },*/
            };

            return infrastructureList;
        }

        public string GetStationName()
        {
            return _stationName;
        }

        public StationTable GetStationTable()
        {
            var stationTable = new StationTable
            {
            };

            return stationTable;
        }
    }
}
