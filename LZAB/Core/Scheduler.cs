﻿using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Třída plánovače
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Scheduler<T>
    {
        private readonly Queue<T> _queue;

        public Scheduler()
        {
            _queue = new Queue<T>();
        }

        /// <summary>
        /// Přidat prvek
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            _queue.Enqueue(item);
        }

        /// <summary>
        /// Získat prvek a smazat
        /// </summary>
        /// <returns></returns>
        public T GetAndRemove()
        {
            return _queue.Count > 0 ? _queue.Dequeue() : default(T);
        }

        /// <summary>
        /// Získat prvek
        /// </summary>
        /// <returns></returns>
        public T Get()
        {
            return _queue.Count > 0 ? _queue.Peek() : default(T);
        }

        /// <summary>
        /// Získat vše
        /// </summary>
        /// <returns></returns>
        public IList<T> GetAll()
        {
            return new List<T>(_queue.ToArray());
        }

        /// <summary>
        /// Vyčistit
        /// </summary>
        public void Clear()
        {
            _queue.Clear();
        }
    }
}