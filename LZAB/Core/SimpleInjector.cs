﻿using Core.Contracts;
using Core.Models;
using Core.Modules;
using Core.Properties;
using SimpleInjector;
using System.Collections.Generic;

namespace Core
{
    /// <summary>
    ///     Třída pro registraci tříd jádra.
    /// </summary>
    public static class SimpleInjector
    {
        /// <summary>
        ///     Registrace
        /// </summary>
        /// <param name="container">Kontajner od Simple Injectoru</param>
        /// <returns>Kontejner</returns>
        public static Container Register(Container container)
        {
            var settings = Settings.Default;

            container.Register<ICore, Core>(Lifestyle.Singleton);
            container.Register<Scheduler<Route>, Scheduler<Route>>(Lifestyle.Singleton);
            container.Register<Scheduler<Action>, Scheduler<Action>>(Lifestyle.Singleton);
            container.Register<ICommunication>(() => new Communication(settings.Port, settings.Baudrate, settings.Timeout), Lifestyle.Singleton);
            //container.Register<ICommunication, CommunicationFake>(Lifestyle.Singleton);
            container.Register<ICommunicationProtokol, CommunicationProtokol>(Lifestyle.Singleton);
            //container.Register<ICommunicationProtokol, FakeCommunicationProtocol>(Lifestyle.Singleton);
            container.Register(() => new List<IModule>
            {
                new TrainModule(),
                new AgreementModule(),
                new RailwayCrossingModule(),
                new RoutesModule(),
                new TrainModule()
            }, Lifestyle.Singleton);

            return container;
        }
    }
}