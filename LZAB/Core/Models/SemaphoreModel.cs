﻿using Core.Models;

public class SemaphoreModel : Infrastructure
{
    public new Signals State
    {
        get { return (Signals)base.State; }
        set { base.State = value; }
    }
}