﻿using System.Collections.Generic;

namespace Core.Models
{
    /// <summary>
    /// Reprezentace záznamu v závěrové tabulce
    /// </summary>
    public class RouteRecord
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public RouteRecord()
        {
            ViaTracks = new List<TrackModel>();
            SwitchesAndPositions = new List<Switch>();
            RailCrossings = new List<RailCrossingModel>();
        }

        /// <summary>
        /// Počáteční kolej
        /// </summary>
        public TrackModel StartTrack { get; set; }

        /// <summary>
        /// Počáteční návěstisdlo
        /// </summary>
        public SemaphoreModel StartSemaphore { get; set; }

        /// <summary>
        /// Průjezdné koleje
        /// </summary>
        public List<TrackModel> ViaTracks { get; set; }

        /// <summary>
        /// Výhybky a jejich polohy v dané cestě
        /// </summary>
        public List<Switch> SwitchesAndPositions { get; set; }

        /// <summary>
        /// Koncová kolej
        /// </summary>
        public TrackModel EndTrack { get; set; }

        /// <summary>
        /// Koncové návěstidlo
        /// </summary>
        public SemaphoreModel EndSemaphore { get; set; }

        /// <summary>
        /// Omezení rychlosti
        /// </summary>
        public int? SpeedLimit { get; set; }

        /// <summary>
        /// Aktivita dané cesty
        /// </summary>
        public bool Active { get; internal set; }

        /// <summary>
        /// Typ
        /// </summary>
        public RouteTypes Type { get; set; }

        /// <summary>
        /// Seznam přejezdů na jejichž uzavření kterých závisí vlaková nebo posunová cesta
        /// </summary>
        public List<RailCrossingModel> RailCrossings { get; set; }
    }
}