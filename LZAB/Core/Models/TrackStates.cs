﻿// ReSharper disable InconsistentNaming

namespace Core.Models
{
    /// <summary>
    /// Výčet stavů kolejí
    /// </summary>
    public enum TrackStates
    {
        UNKNOWN,
        VYL,
        VC,
        PC,
        OBS,
        VOL
    }
}