﻿// ReSharper disable InconsistentNaming

namespace Core.Models
{
    /// <summary>
    /// Výčet typů cest
    /// </summary>
    [System.Flags]
    public enum RouteTypes
    {
        VC,
        PC
    }
}