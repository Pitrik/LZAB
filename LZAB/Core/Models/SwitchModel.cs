﻿using Core.Models;

public class SwitchModel : Infrastructure
{
    public new SwitchStates State
    {
        get { return (SwitchStates)base.State; }
        set { base.State = value; }
    }

    public TrackStates TrackState { get; set; }
}