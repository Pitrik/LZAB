﻿namespace Core.Models
{
    /// <summary>
    /// Reprezentace vlakové cesty
    /// </summary>
    public class Route
    {
        /// <summary>
        /// Konstrukce vlakové cesty
        /// </summary>
        /// <param name="startSemaphore">Počáteční návěstidlo</param>
        /// <param name="endTrack">Cílová kolej</param>
        /// <param name="routeType">Typ</param>
        public Route(Infrastructure startSemaphore, Infrastructure endTrack, RouteTypes routeType)
        {
            StartSemaphore = startSemaphore;
            EndTrack = endTrack;
            RouteType = routeType;
        }

        /// <summary>
        /// Počáteční návěstidlo
        /// </summary>
        public Infrastructure StartSemaphore { get; private set; }

        /// <summary>
        /// Koncová kolej
        /// </summary>
        public Infrastructure EndTrack { get; private set; }

        /// <summary>
        /// Typ
        /// </summary>
        public RouteTypes RouteType { get; private set; }
    }
}