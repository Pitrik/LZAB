﻿using Core.Models;
using System.Drawing;

public class InfrastructureViewModel
{
    public Orientation Orientation { get; set; }

    public Point LocationStart { get; set; }

    public Point LocationEnd { get; set; }
}