﻿// ReSharper disable InconsistentNaming

namespace Core.Models
{
    /// <summary>
    /// Výčet orientací v reliéfu
    /// </summary>
    public enum Orientation
    {
        LR,
        RL
    }
}