﻿using Core.Models;

public class TrackModel : Infrastructure
{
    public new TrackStates State
    {
        get { return (TrackStates)base.State; }
        set { base.State = value; }
    }
}