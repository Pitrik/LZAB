﻿using Core.Models;
using System.Collections.Generic;
using System.Timers;

public class RailCrossingModel : Infrastructure
{
    const int Time = 1000;

    public RailCrossingModel()
    {
        Activators = new List<RailCrossingActivator>();
        Deactivators = new List<TrackModel>();
        Timer = new System.Timers.Timer(Time);
        Timer.Elapsed += Timer_Elapsed;
    }

    private void Timer_Elapsed(object sender, ElapsedEventArgs e)
    {
        State = RailCrossingStates.PreClosed;   //přesun do logiky? Asi ano, ale zatím nevím jak. 
    }

    public new RailCrossingStates State
    {
        get { return (RailCrossingStates)base.State; }
        set { base.State = value; }
    }

    public List<RailCrossingActivator> Activators { get; set; }

    public List<TrackModel> Deactivators { get; set; }

    public string RailwayCrossingTrackName { get; set; }

    public System.Timers.Timer Timer { get; set; }
}