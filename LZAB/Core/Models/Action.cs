﻿namespace Core.Models
{
    /// <summary>
    /// Reprezentace akce.
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Inicialiazace akce
        /// </summary>
        /// <param name="actionType">Typ akce</param>
        /// <param name="infrastructure">Infrastruktura pro akci</param>
        /// <param name="param1">Parametr akce</param>
        public Action(ActionTypes actionType, Infrastructure infrastructure, object param1)
        {
            ActionType = actionType;
            Infrastructure = infrastructure;
            Param1 = param1;
        }

        /// <summary>
        /// Typ akce
        /// </summary>
        public ActionTypes ActionType { get; private set; }

        /// <summary>
        /// Infrastruktura
        /// </summary>
        public Infrastructure Infrastructure { get; private set; }

        /// <summary>
        /// Parametr akce
        /// </summary>
        public object Param1 { get; private set; }
    }
}