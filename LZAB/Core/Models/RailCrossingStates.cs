﻿namespace Core.Models
{
    public enum RailCrossingStates
    {
        Positive,
        Prediction,
        Active,
        PreClosed,
        Closed,
        Unwise,
        //TODO a všechno další
    }
}
