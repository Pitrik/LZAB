﻿// ReSharper disable All
namespace Core.Models
{
    /// <summary>
    /// Typy akcí
    /// </summary>
    public enum ActionTypes
    {
        SetTrainNumber,
        SetAgreement,
        SetLockout,
        CancelLockout,
        CancelRoute,
        PN,
        CancelPN,
        SetSwitchManually,
        LockOrUnlockRailCrossing,
        SetTrackState
    }
}