﻿public class InputOutputInterface
{
    public InputOutputInterface(byte devId, ushort regId)
    {
        DevId = devId;
        RegId = regId;
    }

    public byte DevId { get; set; }

    public ushort RegId { get; set; }

    public byte Read { get; set; }

    public byte Write { get; set; }

    public bool IsUpdated { get; set; }
}