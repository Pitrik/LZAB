﻿namespace Core.Models
{
    /// <summary>
    /// Výčet stavů výhybek
    /// </summary>
    public enum SwitchStates
    {
        L,
        R,
        S
    }
}