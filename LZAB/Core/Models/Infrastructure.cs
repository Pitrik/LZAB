﻿using System;

namespace Core.Models
{
    public abstract class Infrastructure
    {
        private Enum _state;

        public Enum State
        {
            protected get
            {
                var value = _state;

                if (ReadFunction != null && InputInterfaces != null)
                {
                    value = ReadFunction(InputInterfaces.Read, _state);
                }

                return value;
            }
            set
            {
                if (_state == null || !_state.Equals(value))
                {
                    _state = value;

                    if (WriteFunction != null && OutputInterfaces != null)
                    {
                        OutputInterfaces.Write = WriteFunction(_state);
                        OutputInterfaces.IsUpdated = true;
                    }
                }
            }
        }

        public string Name { get; set; }

        public Train Train { get; set; }

        public InputOutputInterface InputInterfaces { get; set; }

        public InputOutputInterface OutputInterfaces { get; set; }

        public InfrastructureViewModel ViewModel { get; set; }

        public Func<Enum, byte> WriteFunction { protected get; set; }

        public Func<byte, Enum, Enum> ReadFunction { protected get; set; }
    }
}