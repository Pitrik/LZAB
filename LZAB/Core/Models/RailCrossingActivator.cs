﻿using System;

namespace Core.Models
{
    public class RailCrossingActivator
    {
        public TrackModel Track { get; set; }

        public SemaphoreModel Semaphore { get; set; }

        public Func<Infrastructure, bool> Expression { get; set; }
    }
}
