﻿// ReSharper disable InconsistentNaming

namespace Core.Models
{
    /// <summary>
    /// Výčet návěstí
    /// </summary>
    public enum Signals
    {
        STUJ,
        VOLNO,
        VOLNO_40,
        VYSTRAHA,
        VYSTRAHA_40,
        VOLNO_40_40,
        VOLNO_OC_40,
        VS_V,
        VS_O,
        PN
    }
}