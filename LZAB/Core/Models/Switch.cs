﻿namespace Core.Models
{
    public class Switch
    {
        public Switch(SwitchModel switchModel, SwitchStates state)
        {
            SwitchModel = switchModel;
            State = state;
        }

        public SwitchModel SwitchModel { get; set; }

        public SwitchStates State { get; set; }
    }
}
