﻿namespace Core.Models
{
    /// <summary>
    /// Reprezentace vlaku
    /// </summary>
    public class Train
    {
        /// <summary>
        /// Číslo vlaku
        /// </summary>
        public string TrainNumber { get; set; }

        /// <summary>
        /// Poznámky nebo popis vlaku
        /// </summary>
        public string TrainDescription { get; set; }
    }
}