﻿using System;
using Core.Contracts;
using Core.Models;

namespace Core
{
    public class CommunicationProtokol : ICommunicationProtokol
    {
        public Enum ReadTrackData(byte data, Enum currentState)
        {
            var mappedCurrentState = (TrackStates)currentState;
            if (mappedCurrentState == TrackStates.UNKNOWN || mappedCurrentState == TrackStates.OBS || mappedCurrentState == TrackStates.VOL)
            {
                if (data == 0x01)
                {
                    return TrackStates.VOL;
                }
                else
                {
                    return TrackStates.OBS;
                }
            }

            return TrackStates.UNKNOWN;
        }

        public byte WriteRailwayCrossingData(Enum data)
        {
            return 0x00;
        }

        public byte WriteSemaphoreData(Enum data)
        {
            return 0x00;
        }

        public byte WriteSwitchData(Enum data)
        {
            return 0x00;
        }
    }
}
