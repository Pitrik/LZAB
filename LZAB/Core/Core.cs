﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Models;
using Action = Core.Models.Action;
using Core.Modules;
using System;

// ReSharper disable FunctionNeverReturns

namespace Core
{
    public class Core : ICore
    {
        private readonly ICommunication _communication;
        private readonly IStation _station;
        private readonly Scheduler<Action> _schedulerForAction;
        private readonly Scheduler<Route> _schedulerForRoutes;
        private readonly IList<Infrastructure> _infrastructureList;
        private readonly IList<RouteRecord> _table;
        private readonly ICommunicationProtokol _protokol;

        //modules
        private readonly List<IModule> _modules;

        public Core(Scheduler<Route> schedulerForRoutes, Scheduler<Action> schedulerForAction, IStation station, ICommunication communication, List<IModule> modules, ICommunicationProtokol protokol)
        {
            _schedulerForRoutes = schedulerForRoutes;
            _schedulerForAction = schedulerForAction;
            _station = station;
            _communication = communication;
            _modules = modules;
            _protokol = protokol;

            _infrastructureList = _station.GetInfrastrucureList();
            _table = _station.GetStationTable();


            //inicializace modulů
            _modules.ForEach(module =>
            {
                module.Init(_schedulerForRoutes, _schedulerForAction, _infrastructureList, _table);
            });

            //Inicializace transformačních funkcí
            var tracks = _infrastructureList.Where(_ => _ is TrackModel).ToList();
            var semaphores = _infrastructureList.Where(_ => _ is SemaphoreModel).ToList();
            var switchs = _infrastructureList.Where(_ => _ is SwitchModel).ToList();
            var railwayCrossings = _infrastructureList.Where(_ => _ is RailCrossingModel).ToList();

            tracks.ForEach(_ => _.ReadFunction = _protokol.ReadTrackData);
            semaphores.ForEach(_ => _.WriteFunction = _protokol.WriteSemaphoreData);
            switchs.ForEach(_ => _.WriteFunction = _protokol.WriteSwitchData);
            railwayCrossings.ForEach(_ => _.WriteFunction = _protokol.WriteRailwayCrossingData);

            Task.Run(Start);
        }

        public IEnumerable<Infrastructure> GetInfrastructureList()
        {
            return _infrastructureList;
        }

        public Infrastructure GetSelectedInfrastructure(int x, int y)
        {
            var result = _infrastructureList.FirstOrDefault(t =>
                t.ViewModel.LocationStart.X <= x &&
                t.ViewModel.LocationStart.Y <= y &&
                t.ViewModel.LocationEnd.X >= x &&
                t.ViewModel.LocationEnd.Y >= y
            );

            return result;
        }

        public IList<Route> GetRoutesInScheduler()
        {
            return _schedulerForRoutes.GetAll();
        }

        public void ClearRoutesInScheduler()
        {
            _schedulerForRoutes.Clear();
        }

        public void AddRouteToScheduler(Infrastructure start, Infrastructure end, RouteTypes type = RouteTypes.VC)
        {
            if (start is SemaphoreModel && end is TrackModel)
                if (_table.ToList().Exists(x => x.StartSemaphore == start && x.EndTrack == end))
                    _schedulerForRoutes.Add(new Route(start, end, type));
        }

        public void AddActionToScheduler(Action action)
        {
            _schedulerForAction.Add(action);
        }

        private async Task Start()
        {
            while (true)
            {
                RunLogic();
                await RunCommunication();
            }
        }

        private async Task RunCommunication()
        {
            //získání kolejí, pro které se čte stav, exkludují se koleje, které se nacházejí ve výluce
            var tracks = _infrastructureList.Where(x => x is TrackModel && (x as TrackModel).State != TrackStates.VYL).ToList();
            var readTasks = tracks.Select(x => _communication.ReadInfrastructureData(x.InputInterfaces)); 

            //zápis stavů návěstidel, výhybek, zápis stavů přejezdů, ... u kterých se provedla změna stavu
            var infrastructureWithoutTracks = _infrastructureList.Where(x => x as TrackModel == null && x.OutputInterfaces?.IsUpdated == true).ToList();
            var writeTasks = infrastructureWithoutTracks.Select(x => _communication.WriteInfrastructureData(x.OutputInterfaces));

            await Task.WhenAll(readTasks);
            await Task.WhenAll(writeTasks);
        }

        private void RunLogic()
        {
            _modules.ForEach(module =>
            {
                module.ExecuteLogic();
                module.ExecuteAction();
            });
        }

        public IList<Action> GetActionsInScheduler()
        {
            return _schedulerForAction.GetAll();
        }

        public void ClearActionsInScheduler()
        {
            _schedulerForAction.Clear();
        }
    }
}