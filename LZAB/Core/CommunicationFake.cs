﻿using Core.Contracts;
using System.Threading.Tasks;
using Core.Models;

namespace Core
{
    class CommunicationFake : ICommunication
    {
#pragma warning disable CS1998 // V této asynchronní metodě chybí operátory await a spustí se synchronně.
        public async Task ReadInfrastructureData(InputOutputInterface inputOutputInterface)
#pragma warning restore CS1998 // V této asynchronní metodě chybí operátory await a spustí se synchronně.
        {
            return;
        }

#pragma warning disable CS1998 // V této asynchronní metodě chybí operátory await a spustí se synchronně.
        public async Task WriteInfrastructureData(InputOutputInterface inputOutputInterface)
#pragma warning restore CS1998 // V této asynchronní metodě chybí operátory await a spustí se synchronně.
        {
            inputOutputInterface.IsUpdated = false;
            return;
        }
    }
}
