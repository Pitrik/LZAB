﻿using Core.Models;

namespace Core
{
    /// <summary>
    /// Rozšiřující metody
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Mapování stavu na hodnoty protokolu
        /// </summary>
        /// <param name="infrastructure"></param>
        /// <returns></returns>
        public static ushort ToModuleState(this Infrastructure infrastructure)
        {
            switch (infrastructure)
            {
                case SemaphoreModel x:
                    switch (x.State)
                    {
                        case Signals.STUJ:
                            return 0x01;
                        case Signals.VOLNO:
                            return 0x02;
                        case Signals.VOLNO_40:
                            return 0x03;
                        case Signals.VOLNO_40_40:
                            return 0x04;
                        case Signals.VOLNO_OC_40:
                            return 0x05;
                        case Signals.VYSTRAHA:
                            return 0x06;
                        case Signals.VYSTRAHA_40:
                            return 0x07;
                        case Signals.VS_O:
                            return 0x08;
                        case Signals.VS_V:
                            return 0x09;
                        case Signals.PN:
                            return 0x0A;
                        default:
                            return 0xFF;
                    }
                case SwitchModel x:
                    switch (x.State)
                    {
                        case SwitchStates.L:
                        case SwitchStates.R:
                            return 0x10;
                        case SwitchStates.S:
                            return 0x11;
                        default:
                            return 0xFF;
                    }
                default:
                    return 0xFF;
            }
        }
    }
}