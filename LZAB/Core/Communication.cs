﻿using System;
using System.IO.Ports;
using Core.Contracts;
using Core.Models;
using System.Threading.Tasks;
using NModbus;
using NModbus.Serial;

namespace Core
{
    public class Communication : ICommunication
    {
        private readonly IModbusMaster _master;

        public Communication(string portName, int baudRate, int timeout)
        {
            var serialPort = new SerialPort
            {
                PortName = portName,
                BaudRate = baudRate,
                WriteTimeout = timeout,
                ReadTimeout = timeout,
                Parity = Parity.None
            };
            serialPort.Open();

            var factory = new ModbusFactory();
            var adapter = new SerialPortAdapter(serialPort);
            _master = factory.CreateAsciiMaster(adapter);
        }

        public Task WriteInfrastructureData(InputOutputInterface outputInterface)
        {
            return Task.Run(() => {
                try
                {
                    _master.WriteSingleRegister(outputInterface.DevId, outputInterface.RegId, outputInterface.Write);
                    outputInterface.IsUpdated = false;
                }
                catch (Exception)
                {
                    outputInterface.IsUpdated = true;
                }

                return;
            });
        }

        public Task ReadInfrastructureData(InputOutputInterface inputInterface)
        {
            return Task.Run(() =>
            {
                try
                {
                    var response = _master.ReadInputRegisters(inputInterface.DevId, inputInterface.RegId, 1);
                    if (response.Length == 1)
                    {
                        inputInterface.Read = (byte)response[0];
                        return;
                    }
                    else
                    {
                        inputInterface.Read = 0x00;
                        return;
                    }

                }
                catch (Exception)
                {
                    inputInterface.Read = 0x00;
                }

                inputInterface.Read = 0x00;
            });
        }
    }
}
