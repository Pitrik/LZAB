﻿using Core.Models;
using System.Collections.Generic;

namespace Core.Modules
{
    public interface IModule
    {
        void Init(Scheduler<Route> schedulerForRoutes, Scheduler<Models.Action> schedulerForAction, IList<Infrastructure> infrastructureList, IList<RouteRecord> stationTable);

        //execute logic
        void ExecuteLogic();
        
        //execute action from action scheduler
        void ExecuteAction();
    }
}
