﻿using Core.Models;
using System;
using System.Linq;

namespace Core.Modules
{
    public class RoutesModule : BaseModule, IModule
    {
        public void ExecuteLogic()
        {
            CheckTrainRoutes();
            SetNewRoutes();
        }

        public void ExecuteAction()
        {
            var action = _schedulerForAction.Get();

            switch (action?.ActionType)
            {
                case ActionTypes.SetTrackState:
                    SetInfrastrucureState(action.Infrastructure, (TrackStates)action.Param1);
                    _schedulerForAction.GetAndRemove();
                    break;
                case ActionTypes.SetSwitchManually:
                    SetInfrastrucureState(action.Infrastructure, (SwitchStates)action.Param1);
                    _schedulerForAction.GetAndRemove();
                    break;
                case ActionTypes.CancelLockout:
                    CancelLockout(action.Infrastructure as TrackModel);
                    _schedulerForAction.GetAndRemove();
                    break;
                case ActionTypes.SetLockout:
                    SetTrackForLockout(action.Infrastructure as TrackModel);
                    _schedulerForAction.GetAndRemove();
                    break;
                case ActionTypes.CancelRoute:
                    CancelTrainRoute(action.Infrastructure.Name);
                    _schedulerForAction.GetAndRemove();
                    break;
                case null:
                default:
                    break;
            }
        }

        private void CheckTrainRoutes()
        {
            var actives = _table.Where(x => x.Active);
            foreach (var active in actives)
            {
                var endSemaphore = active.EndSemaphore;
                var startSemaphore = active.StartSemaphore;
                var endTrack = active.EndTrack;
                var startTrack = active.StartTrack;
                var signalForStartSemaphore = GetSignalForSemaphore(endSemaphore.State, active.SpeedLimit);
                var train = startTrack.Train;
                var switches = active.SwitchesAndPositions;

                //Kontrola zda je poloha výhybek zapsaná na moduly, až poté lze hlavní návěstidlo dát do polohy povolující jízdu vlaku
                SetInfrastrucureState(startSemaphore, switches.Exists(s => s.SwitchModel.OutputInterfaces?.IsUpdated == true) ? Signals.STUJ : signalForStartSemaphore);

                //Kontrola kolejí v cestě -> obsazená kolej v cestě: Active = false, StartSemaphore = STOP
                var viaTracks = active.ViaTracks;
                viaTracks.Add(endTrack);

                if (viaTracks.Exists(x => x.State != TrackStates.VC) ||
                    switches.Exists(s => s.SwitchModel.State != s.State) ||
                    switches.Exists(s => s.SwitchModel.TrackState != TrackStates.VC))
                //existuje v cestě kolej, na které je zrušena VC nebo výhybka je v rozporu s postavenou cestou
                {
                    active.Active = false;
                    SetInfrastrucureState(startSemaphore, Signals.STUJ);
                }

                if (train != null)
                {
                    //číslo vlaku
                    var trackForSetTrain =
                        viaTracks.Where(
                            track =>
                                track.State == TrackStates.VC ||
                                track.State == TrackStates.OBS).ToList();
                    trackForSetTrain.ForEach(track => track.Train = train);
                }
                else
                {
                    //odebírá číslo vlaku
                    var trackForClear = viaTracks.Where(track => track.State == TrackStates.VC).ToList();
                    trackForClear.ForEach(track => track.Train = null);
                }
            }
        }

        private void SetNewRoutes()
        {
            var peek = _schedulerForRoutes.Get();
            if (peek != null)
                switch (peek.RouteType)
                {
                    case RouteTypes.VC:
                        SetNewTrainRoutes(peek);
                        break;
                    case RouteTypes.PC:
                        SetNewShiftRoutes(peek);
                        break;
                    default:
                        throw new Exception();
                }
        }

        private void SetNewTrainRoutes(Route route)
        {
            //nalezne, zda daná cesta existuje v tabulce
            var result = _table.FirstOrDefault(x =>
                x.StartSemaphore == route.StartSemaphore &&
                x.EndTrack == route.EndTrack &&
                x.Type == RouteTypes.VC); //vlaková cesta
            if (result != null)
            {
                //lze aktualně tuto cestu postavit? Pokud ne, zůstává ve frontě a nedělá se dal nic
                //test kolejí, zda nejsou obsazeny a nebo na nich není jiná cesta
                var startSemaphore = result.StartSemaphore;
                var endSemaphore =result.EndSemaphore;
                var endTrack = result.EndTrack;
                var switches = result.SwitchesAndPositions;
                var viaTracks = result.ViaTracks;
                viaTracks.Add(endTrack);

                //lze postavit pouze cestu na volné koleje, na kterých není jiná postavená cesta a z návěstidla je příspustná jen jedna cesta
                if (startSemaphore.State == Signals.STUJ &&
                    viaTracks.All(x => x.State == TrackStates.VOL) &&
                    endSemaphore.State != Signals.VS_V &&
                    switches.All(x => x.SwitchModel.TrackState == TrackStates.VOL))
                {
                    _schedulerForRoutes.GetAndRemove(); //odebere cestu z plánování
                    viaTracks.ForEach(x => SetInfrastrucureState(x, TrackStates.VC)); //nastaví VC na koleje
                    switches.ForEach(x => {
                        SetInfrastrucureState(x.SwitchModel, x.State);
                        SetInfrastrucureState(x.SwitchModel, TrackStates.VC);
                    });
                    result.Active = true; //aktivuje danou cestu
                }
            }
            else
            {
                _schedulerForRoutes.GetAndRemove();
            }
        }

        private void SetNewShiftRoutes(Route route)
        {
            //nalezne, zda daná cesta existuje v tabulce
            var result = _table.FirstOrDefault(x =>
                x.StartSemaphore == route.StartSemaphore &&
                x.EndTrack == route.EndTrack &&
                x.Type == RouteTypes.PC); //posunová cesta
            if (result != null)
            {
                //test zda lze cestu postavit
                //test kolejí
                var startSemaphore = result.StartSemaphore;
                var endTrack = result.EndTrack;
                var switches = result.SwitchesAndPositions;
                var viaTracks = result.ViaTracks;

                //lze postavit pouze cestu přes volné koleje, na kterých není jiná postavená cesta a z návěstidla je příspustná jen jedna cesta
                if (startSemaphore.State == Signals.STUJ &&
                    viaTracks.All(x => x.State == TrackStates.VOL))
                {
                    _schedulerForRoutes.GetAndRemove(); //odebere cestu z plánování
                    viaTracks.ForEach(x => SetInfrastrucureState(x, TrackStates.PC)); //nastavi PC na koleje
                    if (endTrack.State == TrackStates.VOL)
                    {
                        SetInfrastrucureState(endTrack, TrackStates.PC);        //nezruší vlak na cílové koleji, je li tam
                    }
                    switches.ForEach(x => {
                        SetInfrastrucureState(x.SwitchModel, x.State);
                        SetInfrastrucureState(x.SwitchModel, TrackStates.PC);
                    });
                    result.Active = true; //aktivuje danou cestu
                }
            }
            else
            {
                _schedulerForRoutes.GetAndRemove();
            }
        }

        private void SetTrackForLockout(TrackModel track)
        {
            if (track != null && track is TrackModel && track.State == TrackStates.VOL)
                SetInfrastrucureState(track, TrackStates.VYL);
        }

        private void CancelLockout(TrackModel track)
        {
            if (track != null && track is TrackModel && track.State == TrackStates.VYL)
                SetInfrastrucureState(track, TrackStates.VOL);
        }

        private void CancelTrainRoute(string startSemaphore)
        {
            //nalezne cestu
            var result = _table.FirstOrDefault(x => x.Active && x.StartSemaphore.Name == startSemaphore);
            if (result != null)
            {
                SetInfrastrucureState(result.StartSemaphore, Signals.STUJ);
                SetInfrastrucureState(result.EndTrack, TrackStates.VOL);
                result.ViaTracks.ToList().ForEach(x => SetInfrastrucureState(x, TrackStates.VOL));
            }
        }

        private static Signals GetSignalForSemaphore(Signals nextSignal, int? speedLimit = null)
        {
            switch (nextSignal)
            {
                case Signals.STUJ:
                    if (speedLimit == 40)
                        return Signals.VYSTRAHA_40;
                    return Signals.VYSTRAHA;
                case Signals.VOLNO:
                    if (speedLimit == 40)
                        return Signals.VOLNO_40;
                    return Signals.VOLNO;
                case Signals.VOLNO_40:
                    if (speedLimit == 40)
                        return Signals.VOLNO_40_40;
                    return Signals.VOLNO_OC_40;
                case Signals.VYSTRAHA:
                    if (speedLimit == 40)
                        return Signals.VOLNO_40;
                    return Signals.VOLNO;
                case Signals.VYSTRAHA_40:
                    if (speedLimit == 40)
                        return Signals.VOLNO_OC_40;
                    return Signals.VOLNO;
                case Signals.VS_O:
                    if (speedLimit == 40)
                        return Signals.VOLNO_40;
                    return Signals.VOLNO;
                default:
                    return Signals.STUJ;
            }
        }
    }
}
