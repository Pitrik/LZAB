﻿using Core.Models;
using System.Linq;

namespace Core.Modules
{
    public class AgreementModule : BaseModule, IModule
    {
        public void ExecuteAction()
        {
            var action = _schedulerForAction.Get();
            if (action?.ActionType == ActionTypes.SetAgreement)
            {
                SetAgreement(action.Infrastructure, (Signals)action.Param1);
                _schedulerForAction.GetAndRemove();
            }
        }

        public void ExecuteLogic()
        {
            return;
        }

        private void SetAgreement(Infrastructure agreement, Signals signal)
        {
            var subTable = _table.Where(x => x.EndSemaphore == agreement).ToList();
            foreach (var vc in subTable)
            {
                var endTrack = vc.EndTrack;
                var viaTracks = vc.ViaTracks;
                viaTracks.Add(endTrack);
                if (viaTracks.Any(track => track.State != TrackStates.VOL))
                    return;
            }

            SetInfrastrucureState(agreement, signal);
        }
    }
}
