﻿using Core.Models;
using System;
using System.Linq;

namespace Core.Modules
{
    public class RailwayCrossingModule : BaseModule, IModule
    {
        public void ExecuteAction()
        {
            return;
        }

        public void ExecuteLogic()
        {
            var railwayCrossings = _infrastructureList.Where(x => x is RailCrossingModel).Select(x => x as RailCrossingModel).ToList();

            railwayCrossings.ForEach(railwayCrossing =>
            {
                //aktivátory a deaktivátory přejezdu
                var activators = railwayCrossing.Activators;
                var deactivators = railwayCrossing.Deactivators;
                //existuje přejezd v tabulce cest oblasti, tedy má vazbu na SZZ
                var existInRouteTable = _table.Any(x => x.RailCrossings.Any(y => y == railwayCrossing));
                //stav přejezdu pro inicializaci FSM
                var state = railwayCrossing.State;
                var nextState = state;

                switch (state)
                {
                    case RailCrossingStates.Active:
                        //čekání na předzváněčku
                        railwayCrossing.Timer.Start();
                        //po uplnyutí času se změní stav přejezdu, děje se tak v modelu. 
                        break;
                    case RailCrossingStates.Closed:
                        //uvolnění přejezdu
                        if ((GetInfrastructureByName(railwayCrossing.RailwayCrossingTrackName) as TrackModel).State == TrackStates.VOL)
                        {
                            nextState = RailCrossingStates.Unwise;
                        }
                        break;
                    case RailCrossingStates.Positive:
                        if (existInRouteTable)
                        {
                            //existuje postavená cesta na přejezd
                            if (activators.Select(x => x.Semaphore).Any(x => x.State != Signals.STUJ))
                            {
                                nextState = RailCrossingStates.Prediction;
                            }
                        }
                        else
                        {
                            //je obsazena aktivační kolej přejezdu
                            if (activators.Any(x => x.Track.State == TrackStates.OBS))
                            {
                                nextState = RailCrossingStates.Active;
                            }
                        }
                        break;
                    case RailCrossingStates.PreClosed:
                        railwayCrossing.Timer.Stop();
                        //obsazení přejezdu
                        if ((GetInfrastructureByName(railwayCrossing.RailwayCrossingTrackName) as TrackModel).State == TrackStates.OBS)
                        {
                            nextState = RailCrossingStates.Closed;
                        }
                        break;
                    case RailCrossingStates.Prediction:
                        //obsazení aktivační koleje
                        if (activators.Any(x => x.Track.State == TrackStates.OBS))
                        {
                            nextState = RailCrossingStates.Active;
                        }
                        break;
                    case RailCrossingStates.Unwise:
                        if (existInRouteTable)
                        {

                        }
                        else
                        {
                            
                        }
                        break;
                    default:
                        throw new Exception("");
                }

                //nastavení přejezdu do nového stavu
                if (nextState != state)
                {
                    railwayCrossing.State = state;
                }
            });
        }
    }
}
