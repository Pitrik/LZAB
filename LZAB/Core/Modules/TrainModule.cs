﻿using Core.Models;
using System.Linq;

namespace Core.Modules
{
    public class TrainModule : BaseModule, IModule
    {
        public void ExecuteAction()
        {
            var action = _schedulerForAction.Get();
            if (action?.ActionType == ActionTypes.SetTrainNumber)
            {
                SetTrainNumber(action.Infrastructure.Name, action.Param1 as string);
                _schedulerForAction.GetAndRemove();
            }
        }

        public void ExecuteLogic()
        {
            var allTracks = _infrastructureList.Where(i => i is TrackModel).Select(x => x as TrackModel);
            var tracksForClean = allTracks.Where(track => track.State != TrackStates.VC && track.State != TrackStates.OBS).ToList();
            tracksForClean.ForEach(track => track.Train = null);
        }

        private void SetTrainNumber(string trackName, string trainNumber)
        {
            var track = GetInfrastructureByName(trackName) as TrackModel;
            if (track.State != TrackStates.OBS)
                return;

            track.Train = new Train { TrainNumber = trainNumber };
        }
    }
}
