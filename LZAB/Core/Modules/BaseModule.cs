﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Modules
{
    public abstract class BaseModule
    {
        protected IList<Infrastructure> _infrastructureList;
        protected IList<RouteRecord> _table;
        protected Scheduler<Route> _schedulerForRoutes;
        protected Scheduler<Models.Action> _schedulerForAction;

        public void Init(Scheduler<Route> schedulerForRoutes, Scheduler<Models.Action> schedulerForAction, IList<Infrastructure> infrastructureList, IList<RouteRecord> stationTable)
        {
            _infrastructureList = infrastructureList;
            _table = stationTable;
            _schedulerForAction = schedulerForAction;
            _schedulerForRoutes = schedulerForRoutes;
        }

        protected Infrastructure GetInfrastructureByName(string name)
        {
            return _infrastructureList.FirstOrDefault(x => x.Name == name);
        }

        protected void SetInfrastrucureState(Infrastructure infrastructure, Enum state)
        {
            if (infrastructure is SwitchModel && state is TrackStates)
                (infrastructure as SwitchModel).TrackState = (TrackStates)state;
            else 
                infrastructure.State = state;
        }
    }
}
