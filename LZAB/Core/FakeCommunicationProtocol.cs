﻿using Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    class FakeCommunicationProtocol : ICommunicationProtokol
    {
        public Enum ReadTrackData(byte data, Enum currentState)
        {
            return currentState;
        }

        public byte WriteRailwayCrossingData(Enum data)
        {
            return 0x00;
        }

        public byte WriteSemaphoreData(Enum data)
        {
            return 0x00;
        }

        public byte WriteSwitchData(Enum data)
        {
            return 0x00;
        }
    }
}
