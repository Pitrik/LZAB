﻿using System;

namespace Core.Contracts
{
    public interface ICommunicationProtokol
    {
        Enum ReadTrackData(byte data, Enum currentState);

        byte WriteSemaphoreData(Enum data);

        byte WriteSwitchData(Enum data);

        byte WriteRailwayCrossingData(Enum data);

        //...
    }
}
