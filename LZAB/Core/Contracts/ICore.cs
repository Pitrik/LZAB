﻿using System.Collections.Generic;
using Core.Models;

namespace Core.Contracts
{
    /// <summary>
    ///     Interface definující rozhraní pro práci s logikou.
    /// </summary>
    public interface ICore
    {
        /// <summary>
        ///     Vrácení seznamu infrastruktury.
        /// </summary>
        /// <returns>Seznam infrastruktury</returns>
        IEnumerable<Infrastructure> GetInfrastructureList();

        /// <summary>
        ///     Vrací infrastrukturu na zadaných souřadnicích.
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>Infrastruktura</returns>
        Infrastructure GetSelectedInfrastructure(int x, int y);

        /// <summary>
        ///     Získání záznamů v plánovači vlakových cest.
        /// </summary>
        /// <returns>Položky plánovače</returns>
        IList<Route> GetRoutesInScheduler();

        IList<Action> GetActionsInScheduler();

        /// <summary>
        ///     Vyčištění plánovače vlakových cest.
        /// </summary>
        void ClearRoutesInScheduler();

        void ClearActionsInScheduler();

        /// <summary>
        ///     Přidání cesty do plánovače vlakových cest.
        /// </summary>
        /// <param name="start">Návěstidlo počátku vlakové cesty</param>
        /// <param name="end">Koncová kolej vlakové cesty</param>
        /// <param name="type">Typ cesty</param>
        void AddRouteToScheduler(Infrastructure start, Infrastructure end, RouteTypes type);

        /// <summary>
        ///     Přidá akci do plánovače akcí.
        /// </summary>
        /// <param name="action">Akce k provedení</param>
        void AddActionToScheduler(Action action);
    }
}