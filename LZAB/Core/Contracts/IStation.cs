﻿using System.Collections.Generic;
using Core.Models;

namespace Core.Contracts
{
    /// <summary>
    ///     Interface definující rozhraní stanice.
    /// </summary>
    public interface IStation
    {
        /// <summary>
        ///     Metoda vracející uzávěrovou tabulku.
        /// </summary>
        /// <returns>Uzávěrová tabulka</returns>
        IList<RouteRecord> GetStationTable();

        /// <summary>
        ///     Metoda vracející seznam infrastruktury.
        /// </summary>
        /// <returns>Seznam infrastruktury</returns>
        IList<Infrastructure> GetInfrastrucureList();

        /// <summary>
        ///     Metoda vracející název stanice, oblasti.
        /// </summary>
        /// <returns>Název stanice</returns>
        string GetStationName();
    }
}