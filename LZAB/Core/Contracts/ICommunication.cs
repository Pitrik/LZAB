﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Contracts
{
    /// <summary>
    /// Interface definující rozhraní pro komunikaci s HW.
    /// </summary>
    public interface ICommunication
    {
        /// <summary>
        /// Zápis dat do HW.
        /// </summary>
        /// <param name="infrastructure">Infrastruktura</param>
        Task WriteInfrastructureData(InputOutputInterface inputOutputInterface);
        
        /// <summary>
        /// Čtení dat z HW.
        /// </summary>
        /// <param name="infrastructure">Infrastruktura</param>
        Task ReadInfrastructureData(InputOutputInterface inputOutputInterface);
    }
}