﻿using System;
using System.Windows.Forms;

namespace LZAB
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                var container = SimpleInjector.Booster();
                var mainForm = container.GetInstance<mainForm>();

                Application.Run(mainForm);
            }
            catch (Exception)
            {
                MessageBox.Show("Nelze inicializovat moduly. Zkontrolujte konfiguraci.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}