﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Core.Contracts;
using Core.Models;
using LZAB.Extensions;
using Action = Core.Models.Action;

namespace LZAB
{
    public partial class mainForm : Form
    {
        private Infrastructure _lastSelected;
        private readonly ICore _logics;

        private Infrastructure _selected;
        private Color _selectedColor;
        private bool _setTrainNumber;
        private string _textInput;
        private bool _textInputActive;

        public mainForm(ICore logics)
        {
            _logics = logics;

            InitializeComponent();

            _textInputActive = false;
            _setTrainNumber = false;
            _textInput = string.Empty;

            timer.Start();
        }

        private void mainPanel_Click(object sender, EventArgs e)
        {
            var mouseEvent = e as MouseEventArgs;

            if (mouseEvent != null)
            {
                var x = mouseEvent.X / LzabGraphics.GridSize;
                var y = mouseEvent.Y / LzabGraphics.GridSize;

                _selected = _logics.GetSelectedInfrastructure(x, y);

                if (_selected != null)
                {
                    if (mouseEvent.Button == MouseButtons.Left)
                    {
                        if (_lastSelected == null)
                        {
                            _lastSelected = _selected;
                            _selectedColor = Color.Green;
                        }
                        else
                        {
                            _logics.AddRouteToScheduler(_lastSelected, _selected, RouteTypes.VC);
                            _selected = _lastSelected = null;
                        }
                    }
                    else if (mouseEvent.Button == MouseButtons.Right)
                    {
                        _lastSelected = null;

                        ShowContextMenu(new Point(mouseEvent.X, mouseEvent.Y));
                    }
                    else if (mouseEvent.Button == MouseButtons.Middle)
                    {
                        if (_lastSelected == null)
                        {
                            _lastSelected = _selected;
                            _selectedColor = Color.White;
                        }
                        else
                        {
                            _logics.AddRouteToScheduler(_lastSelected, _selected, RouteTypes.PC);
                            _selected = _lastSelected = null;
                        }
                    }
                }
                else
                {
                    _selected = null;

                    if (mouseEvent.Button == MouseButtons.Right)
                        ShowContextMenu(new Point(mouseEvent.X, mouseEvent.Y));
                }
            }
        }

        private void ShowContextMenu(Point position)
        {
            contextMenu.Items.Clear();

            var info = $"{_selected?.Name}";
            contextMenu.Items.Add(info).Enabled = false;

            switch (_selected)
            {
                case SemaphoreModel x:
                    contextMenu.Items.Add("RVC");
                    contextMenu.Items.Add("PN");
                    contextMenu.Items.Add("CPN");
                    contextMenu.Items.Add("ODJEZD");
                    contextMenu.Items.Add("VJEZD");
                    break;
                case SwitchModel x:
                    contextMenu.Items.Add("/");
                    contextMenu.Items.Add("\\");
                    contextMenu.Items.Add("|");
                    contextMenu.Items.Add("SIMVOL");
                    contextMenu.Items.Add("SIMOBS");
                    break;
                case TrackModel x:
                    contextMenu.Items.Add("CISLO");
                    contextMenu.Items.Add("PD");
                    contextMenu.Items.Add("VYL");
                    contextMenu.Items.Add("RVYL");
                    contextMenu.Items.Add("SIMVOL");
                    contextMenu.Items.Add("SIMOBS");
                    break;
                case null:
                    contextMenu.Items.Add("CLCFIFO");
                    contextMenu.Items.Add("DIAG");
                    break;
            }

            contextMenu.Show(this, position.X, position.Y);
        }

        private void DrawDiagram(Graphics gPanel)
        {
            var infrastructureList = _logics.GetInfrastructureList();
            foreach (var infrastructure in infrastructureList)
                switch (infrastructure)
                {
                    case SemaphoreModel x:
                        gPanel.DrawSemaphore(infrastructure as SemaphoreModel);
                        break;
                    case SwitchModel x:
                        gPanel.DrawSwitch(infrastructure as SwitchModel);
                        break;
                    case TrackModel x:
                        gPanel.DrawTrack(infrastructure as TrackModel);
                        break;
                }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            var now = DateTime.Now;

            var graphics = mainPanel.CreateGraphics();
            graphics.Clear(Color.Black);

            if (_lastSelected != null)
                graphics.DrawSelected(_lastSelected.ViewModel.LocationStart, _selectedColor);

            //UI
            graphics.DrawTime(new Point(1, 0), now);
            graphics.DrawText(new Point(1, 15), "FIFO");

            if (_textInputActive)
                graphics.DrawText(new Point(1, 26), ">>" + _textInput, Color.Yellow);

            var vcInScheduler = _logics.GetRoutesInScheduler();
            for (var i = 0; i < vcInScheduler.Count; ++i)
                graphics.DrawText(new Point(1, 16 + i),
                    vcInScheduler[i].StartSemaphore + " -> " + vcInScheduler[i].EndTrack + " " +
                    vcInScheduler[i].RouteType);

            DrawDiagram(graphics);
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "VYL")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetLockout, _selected, null));

            else if (e.ClickedItem.Text == "RVYL")
                _logics.AddActionToScheduler(new Action(ActionTypes.CancelLockout, _selected, null));

            else if (e.ClickedItem.Text == "ODJEZD")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetAgreement, _selected, Signals.VS_O));

            else if (e.ClickedItem.Text == "VJEZD")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetAgreement, _selected, Signals.VS_V));

            else if (e.ClickedItem.Text == "RVC")
                _logics.AddActionToScheduler(new Action(ActionTypes.CancelRoute, _selected, null));

            else if (e.ClickedItem.Text == "CLCFIFO")
                _logics.ClearRoutesInScheduler();

            else if (e.ClickedItem.Text == "CISLO")
            {
                _textInputActive = true;
                _setTrainNumber = true;
            }

            else if (e.ClickedItem.Text == "PD")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetTrainNumber, _selected, string.Empty));

            else if (e.ClickedItem.Text == "PN")
                _logics.AddActionToScheduler(new Action(ActionTypes.PN, _selected, null));

            else if (e.ClickedItem.Text == "CPN")
                _logics.AddActionToScheduler(new Action(ActionTypes.CancelPN, _selected, null));

            else if (e.ClickedItem.Text == "/")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetSwitchManually, _selected, SwitchStates.R));

            else if (e.ClickedItem.Text == "|")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetSwitchManually, _selected, SwitchStates.S));

            else if (e.ClickedItem.Text == "\\")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetSwitchManually, _selected, SwitchStates.L));

            else if (e.ClickedItem.Text == "SIMVOL")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetTrackState, _selected, TrackStates.VOL));

            else if (e.ClickedItem.Text == "SIMOBS")
                _logics.AddActionToScheduler(new Action(ActionTypes.SetTrackState, _selected, TrackStates.OBS));

        }

        private void mainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_textInputActive)
            { 
                if (e.KeyChar == '\b' && _textInput.Length > 0)
                {
                    _textInput = _textInput.Remove(_textInput.Length - 1);
                }
                else if (e.KeyChar == '\r')
                {
                    _textInputActive = false;

                    if (_setTrainNumber)
                    {
                        _logics.AddActionToScheduler(new Action(ActionTypes.SetTrainNumber, _selected, _textInput));
                        _setTrainNumber = false;
                    }

                    _textInput = string.Empty;
                }
                else
                {
                    _textInput = _textInput.Insert(_textInput.Length, e.KeyChar.ToString());
                }
            }
        }
    }
}