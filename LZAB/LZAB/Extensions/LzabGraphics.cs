﻿using System;
using System.Drawing;
using System.Globalization;
using Core.Models;

namespace LZAB.Extensions
{
    /// <summary>
    /// Rozšíření grafických metod
    /// </summary>
    public static class LzabGraphics
    {
        /// <summary>
        /// Velikost mřížky
        /// </summary>
        public const int GridSize = 20;

        /// <summary>
        /// Vykreslení návěstidla
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="semaphore"></param>
        public static void DrawSemaphore(this Graphics gPanel, SemaphoreModel semaphore)
        {
            var color = Color.Gray;
            switch (semaphore.State)
            {
                case Signals.STUJ:
                    color = Color.Gray;
                    break;
                case Signals.VOLNO:
                case Signals.VOLNO_40:
                case Signals.VOLNO_40_40:
                case Signals.VYSTRAHA:
                case Signals.VYSTRAHA_40:
                    color = Color.Green;
                    break;
                case Signals.PN:
                    color = Color.Blue;
                    break;
            }

            var newPosition = ConvertMatrixCoorfinatesToPixel(semaphore.ViewModel.LocationStart);

            Point[] points;
            if (semaphore.ViewModel.Orientation == Orientation.LR)
                points = new[]
                {
                    new Point(newPosition.X, newPosition.Y + 3),
                    new Point(newPosition.X + GridSize, newPosition.Y + GridSize / 2),
                    new Point(newPosition.X, newPosition.Y + GridSize - 3)
                };
            else
                points = new[]
                {
                    new Point(newPosition.X + GridSize, newPosition.Y + 3),
                    new Point(newPosition.X, newPosition.Y + GridSize / 2),
                    new Point(newPosition.X + GridSize, newPosition.Y + GridSize - 3)
                };

            gPanel.FillPolygon(new SolidBrush(color), points);
        }

        /// <summary>
        /// Vykreslení TS
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="agreement"></param>
        public static void DrawAgreement(this Graphics gPanel, SemaphoreModel agreement)
        {
            var newPosition = ConvertMatrixCoorfinatesToPixel(agreement.ViewModel.LocationStart);

            if (agreement.State == Signals.VS_V && agreement.ViewModel.Orientation == Orientation.LR)
            {
                var pen = new Pen(Color.Gray, 2);
                gPanel.DrawLine(pen, newPosition.X, newPosition.Y, newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y - GridSize / 2 + 3,
                    newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y + GridSize / 2 - 3,
                    newPosition.X + GridSize, newPosition.Y);
            }
            else if (agreement.State == Signals.VS_O && agreement.ViewModel.Orientation == Orientation.RL)
            {
                var pen = new Pen(Color.Blue, 2);
                gPanel.DrawLine(pen, newPosition.X, newPosition.Y, newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y - GridSize / 2 + 3,
                    newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y + GridSize / 2 - 3,
                    newPosition.X + GridSize, newPosition.Y);
            }
            else if (agreement.State == Signals.VS_V && agreement.ViewModel.Orientation == Orientation.RL)
            {
                var pen = new Pen(Color.Gray, 2);
                gPanel.DrawLine(pen, newPosition.X, newPosition.Y, newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y - GridSize / 2 + 3, newPosition.X,
                    newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y + GridSize / 2 - 3, newPosition.X,
                    newPosition.Y);
            }
            else
            {
                var pen = new Pen(Color.Blue, 2);
                gPanel.DrawLine(pen, newPosition.X, newPosition.Y, newPosition.X + GridSize, newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y - GridSize / 2 + 3, newPosition.X,
                    newPosition.Y);
                gPanel.DrawLine(pen, newPosition.X + GridSize / 2, newPosition.Y + GridSize / 2 - 3, newPosition.X,
                    newPosition.Y);
            }
        }

        /// <summary>
        /// Vykreslení koleje
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="track"></param>
        public static void DrawTrack(this Graphics gPanel, TrackModel track)
        {
            var color = Color.Gray;
            switch (track.State)
            {
                case TrackStates.OBS:
                    color = Color.Red;
                    break;
                case TrackStates.UNKNOWN:
                    color = Color.Purple;
                    break;
                case TrackStates.VC:
                    color = Color.Green;
                    break;
                case TrackStates.VOL:
                    color = Color.Gray;
                    break;
                case TrackStates.VYL:
                    color = Color.Yellow;
                    break;
                case TrackStates.PC:
                    color = Color.White;
                    break;
            }

            var train = track.Train?.TrainNumber;

            var newStartPosition = ConvertMatrixCoorfinatesToPixel(track.ViewModel.LocationStart);
            var newEndPosition = ConvertMatrixCoorfinatesToPixel(track.ViewModel.LocationEnd);
            newStartPosition.Y += GridSize / 2;
            newEndPosition.Y += GridSize / 2;
            newEndPosition.X += GridSize;

            gPanel.DrawLine(new Pen(color, 3), newStartPosition, newEndPosition);
            gPanel.DrawText(new Point((track.ViewModel.LocationStart.X + track.ViewModel.LocationEnd.X) / 2, track.ViewModel.LocationStart.Y),
                $"{track.Name} {train}", fontColor: color);
        }

        /// <summary>
        /// Vykreslení výhybky
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="@switch"></param>
        public static void DrawSwitch(this Graphics gPanel, SwitchModel @switch)
        {
            var newStartPosition = ConvertMatrixCoorfinatesToPixel(@switch.ViewModel.LocationStart);
            var newEndPosition = ConvertMatrixCoorfinatesToPixel(@switch.ViewModel.LocationEnd);
            newEndPosition.X += GridSize;
            var color = Color.Gray;

            switch (@switch.TrackState)
            {
                case TrackStates.OBS:
                    color = Color.Red;
                    break;
                case TrackStates.PC:
                    color = Color.White;
                    break;
                case TrackStates.UNKNOWN:
                    color = Color.Purple;
                    break;
                case TrackStates.VC:
                    color = Color.Green;
                    break;
                case TrackStates.VOL:
                    break;
                case TrackStates.VYL:
                    color = Color.Yellow;
                    break;
            }

            if (@switch.State == SwitchStates.L)
            {
                newStartPosition.Y += GridSize / 2;
                newEndPosition.Y -= GridSize / 2;

                if (@switch.ViewModel.Orientation == Orientation.RL)
                {
                    newStartPosition.Y += GridSize;
                    newEndPosition.Y += GridSize;
                }
            }
            else if (@switch.State == SwitchStates.R)
            {
                newStartPosition.Y += GridSize / 2;
                newEndPosition.Y += GridSize + GridSize / 2;

                if (@switch.ViewModel.Orientation == Orientation.RL)
                {
                    newStartPosition.Y -= GridSize;
                    newEndPosition.Y -= GridSize;
                }
            }
            else if (@switch.State == SwitchStates.S)
            {
                newStartPosition.Y += GridSize / 2;
                newEndPosition.Y += GridSize / 2;
            }

            gPanel.DrawLine(new Pen(color, 3), newStartPosition, newEndPosition);
        }

        /// <summary>
        /// Vykreslení času
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="position"></param>
        /// <param name="dateTime"></param>
        public static void DrawTime(this Graphics gPanel, Point position, DateTime dateTime)
        {
            var newPosition = ConvertMatrixCoorfinatesToPixel(position);
            var text = dateTime.ToString(CultureInfo.InvariantCulture);
            var font = new Font(FontFamily.GenericMonospace, GridSize / (float) 1.5, FontStyle.Regular);
            var size = gPanel.MeasureString(text, font);
            gPanel.FillRectangle(new SolidBrush(Color.DarkBlue), 0, 0, gPanel.ClipBounds.Width,
                newPosition.Y + size.Height);
            gPanel.DrawString(text, font, new SolidBrush(Color.White), newPosition);
        }

        /// <summary>
        /// Vykreslení textu
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="position"></param>
        /// <param name="text"></param>
        /// <param name="backgroundColor"></param>
        /// <param name="fontColor"></param>
        public static void DrawText(this Graphics gPanel, Point position, string text, Color? backgroundColor = null,
            Color? fontColor = null)
        {
            if (!backgroundColor.HasValue)
                backgroundColor = Color.Black;

            if (!fontColor.HasValue)
                fontColor = Color.Gray;

            var newPosition = ConvertMatrixCoorfinatesToPixel(position);
            var font = new Font(FontFamily.GenericMonospace, GridSize / 2, FontStyle.Regular);
            var size = gPanel.MeasureString(text, font);
            gPanel.FillRectangle(new SolidBrush(backgroundColor.Value), newPosition.X, newPosition.Y, size.Width,
                size.Height);
            gPanel.DrawString(text, font, new SolidBrush(fontColor.Value), newPosition);
        }

        /// <summary>
        /// Vykreslení výběru
        /// </summary>
        /// <param name="gPanel"></param>
        /// <param name="position"></param>
        /// <param name="color"></param>
        public static void DrawSelected(this Graphics gPanel, Point position, Color color)
        {
            var newPosition = ConvertMatrixCoorfinatesToPixel(position);

            var points = new[]
            {
                new Point(newPosition.X, newPosition.Y),
                new Point(newPosition.X + GridSize, newPosition.Y),
                new Point(newPosition.X + GridSize, newPosition.Y + GridSize),
                new Point(newPosition.X, newPosition.Y + GridSize)
            };

            gPanel.DrawPolygon(new Pen(color), points);
        }

        private static Point ConvertMatrixCoorfinatesToPixel(Point point)
        {
            return new Point
            {
                X = point.X * GridSize,
                Y = point.Y * GridSize
            };
        }
    }
}