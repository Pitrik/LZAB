﻿using Core.Contracts;
using SimpleInjector;

namespace LZAB
{
    public static class SimpleInjector
    {
        public static Container Booster()
        {
            var container = new Container();

            container.Register<IStation, ExampleStation.ExampleStation>(Lifestyle.Singleton);

            Core.SimpleInjector.Register(container);
                       
            container.Verify();

            return container;
        }
    }
}