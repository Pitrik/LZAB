<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="PaJa" color="12" fill="7" visible="yes" active="yes"/>
<layer number="101" name="Doplnky" color="5" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Kola" color="11" fill="7" visible="yes" active="yes"/>
<layer number="103" name="Popisy" color="2" fill="8" visible="yes" active="yes"/>
<layer number="104" name="Zapojeni" color="6" fill="7" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="#pitrik1">
<description>Librery of Pitrik Martin</description>
<packages>
<package name="ARK300-2">
<pad name="1" x="0" y="2" drill="1" diameter="3.81" shape="long"/>
<pad name="2" x="0" y="-3" drill="1" diameter="3.81" shape="long"/>
<wire x1="-5" y1="-6" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-4" y2="5" width="0.127" layer="21"/>
<wire x1="-4" y1="5" x2="4" y2="5" width="0.127" layer="21"/>
<wire x1="4" y1="5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="-6" width="0.127" layer="21"/>
<wire x1="5" y1="-6" x2="4" y2="-6" width="0.127" layer="21"/>
<wire x1="4" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-5" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-4" y1="-6" x2="-4" y2="5" width="0.127" layer="21"/>
<wire x1="4" y1="-6" x2="4" y2="5" width="0.127" layer="21"/>
<circle x="0" y="2" radius="1" width="0.127" layer="21"/>
<circle x="0" y="-3" radius="1" width="0.127" layer="21"/>
</package>
<package name="ARK300-3">
<pad name="3" x="0" y="-5" drill="1" diameter="3.81" shape="long"/>
<pad name="1" x="0" y="5" drill="1" diameter="3.81" shape="long"/>
<pad name="2" x="0" y="0" drill="1" diameter="3.81" shape="long"/>
<wire x1="-4" y1="8" x2="4" y2="8" width="0.127" layer="21"/>
<wire x1="4" y1="8" x2="5" y2="8" width="0.127" layer="21"/>
<wire x1="5" y1="8" x2="5" y2="-8" width="0.127" layer="21"/>
<wire x1="5" y1="-8" x2="4" y2="-8" width="0.127" layer="21"/>
<wire x1="-4" y1="-8" x2="-5" y2="-8" width="0.127" layer="21"/>
<wire x1="-5" y1="-8" x2="-5" y2="8" width="0.127" layer="21"/>
<wire x1="-5" y1="8" x2="-4" y2="8" width="0.127" layer="21"/>
<wire x1="-4" y1="8" x2="-4" y2="-8" width="0.127" layer="21"/>
<wire x1="-4" y1="-8" x2="4" y2="-8" width="0.127" layer="21"/>
<wire x1="4" y1="-8" x2="4" y2="8" width="0.127" layer="21"/>
<circle x="0" y="5" radius="1" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<circle x="0" y="-5" radius="1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ARK300-2">
<pin name="1" x="-5.08" y="0" visible="pad" length="middle"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-3.81" x2="1.778" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.778" y1="-3.81" x2="1.778" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.27" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="2" x="-5.08" y="-2.54" visible="pad" length="middle"/>
<circle x="0.508" y="-2.54" radius="0.567959375" width="0.254" layer="94"/>
<circle x="0.508" y="0" radius="0.567959375" width="0.254" layer="94"/>
<wire x1="0.254" y1="-2.286" x2="0.762" y2="-2.794" width="0.254" layer="94"/>
<wire x1="0.254" y1="0.254" x2="0.762" y2="-0.254" width="0.254" layer="94"/>
<text x="-7.112" y="1.524" size="1.778" layer="94">&gt;NAME</text>
<text x="-7.112" y="-5.842" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="ARK300-3">
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle"/>
<pin name="2" x="-7.62" y="0" visible="pad" length="middle"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<circle x="-1.27" y="2.54" radius="0.71841875" width="0.254" layer="94"/>
<circle x="-1.27" y="-2.54" radius="0.71841875" width="0.254" layer="94"/>
<circle x="-1.27" y="0" radius="0.71841875" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.794" x2="-0.762" y2="2.286" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.254" x2="-0.762" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.286" x2="-0.762" y2="-2.794" width="0.254" layer="94"/>
<text x="-9.398" y="4.318" size="1.27" layer="94">&gt;NAME</text>
<text x="-9.652" y="-5.588" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARK300-2" prefix="X">
<description>Svorkovnice ARK 300-2</description>
<gates>
<gate name="G$1" symbol="ARK300-2" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="ARK300-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ARK300-3" prefix="X">
<description>Svorkovnice ARK 300-2</description>
<gates>
<gate name="G$1" symbol="ARK300-3" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="ARK300-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#_GM99">
<packages>
<package name="DO41">
<wire x1="1.5001" y1="-1.2499" x2="1.5001" y2="1.2499" width="0.127" layer="21"/>
<wire x1="1.5001" y1="1.2499" x2="6.5001" y2="1.2499" width="0.127" layer="21"/>
<wire x1="6.5001" y1="1.2499" x2="6.5001" y2="-1.2499" width="0.127" layer="21"/>
<wire x1="6.5001" y1="-1.2499" x2="1.5001" y2="-1.2499" width="0.127" layer="21"/>
<pad name="K" x="0" y="0" drill="1.016" diameter="2.54" shape="square"/>
<pad name="A" x="8" y="0" drill="1.016" diameter="2.54"/>
<text x="1" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="0.7501" y="-3.2499" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.905" y1="-1.27" x2="2.286" y2="1.27" layer="21"/>
</package>
<package name="TO-220S">
<wire x1="-2.413" y1="-1.905" x2="7.493" y2="-1.905" width="0.127" layer="21"/>
<wire x1="7.493" y1="-1.905" x2="7.493" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.905" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.9304"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.9304"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.9304"/>
<text x="-1.905" y="3.175" size="1.778" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.445" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-2.54" y1="1.27" x2="7.62" y2="2.54" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="78XX">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.985" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="4.445" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<pin name="VI" x="-10.16" y="0" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="VO" x="10.16" y="0" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4007" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="DO41">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="7805" prefix="U" uservalue="yes">
<gates>
<gate name="G$13" symbol="78XX" x="10.16" y="0"/>
</gates>
<devices>
<device name="" package="TO-220S">
<connects>
<connect gate="G$13" pin="GND" pad="2"/>
<connect gate="G$13" pin="VI" pad="1"/>
<connect gate="G$13" pin="VO" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_16">
<description>&lt;B&gt;PaJa 16&lt;/B&gt; - knihovna   &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 
&lt;I&gt;(vytvoreno 4.12.2005)&lt;/I&gt;&lt;BR&gt;
Univerzální knihovna soucastek do Eagle &lt;I&gt;(od verze 4.11)&lt;/I&gt;&lt;BR&gt;
&lt;BR&gt;
Knihovna obsahuje: 225 soucastek na DPS, 280 do SCHematu&lt;BR&gt;
&lt;BR&gt;
&lt;Author&gt;Copyright (C) PaJa 2001-2005&lt;BR&gt;
http://www.paja-trb.unas.cz&lt;BR&gt;
paja-trb@seznam.cz
&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.7463" y1="-0.0001" x2="1.7463" y2="-0.0001" width="0.6096" layer="94"/>
<text x="-1.1113" y="0.3175" size="0.254" layer="100">PaJa</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;B&gt;SCH symbol&lt;/B&gt; - zem - &lt;I&gt;GrouND&lt;/I&gt;</description>
<gates>
<gate name="ZEM" symbol="GND" x="-45.72" y="35.56"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#Kondenzatory">
<description>&lt;B&gt;Vlada&lt;/B&gt; - knihovna  
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;I&gt;vytvoreno 2.2.2005&lt;/I&gt;&lt;BR&gt;
Univerzalni knihovna soucastek do Eagle &lt;I&gt;(od verze 4.13)&lt;/I&gt;&lt;BR&gt;
&lt;BR&gt;

&lt;Author&gt;Copyright (C) Vlada 2004-2005&lt;BR&gt;</description>
<packages>
<package name="ELEKTROLITICKY_RM1,5-4">
<wire x1="0.635" y1="1.27" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.5875" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.27" x2="0.3175" y2="0.9525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2" width="0.127" layer="21"/>
<pad name="P$1" x="-0.8" y="0" drill="0.8" diameter="1.016" shape="long" rot="R90"/>
<pad name="P$2" x="0.8" y="0" drill="0.8" diameter="1.016" shape="long" rot="R90"/>
<text x="-2.5875" y="2.3145" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.576" y="-3.4763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM2,5-5">
<wire x1="0.635" y1="1.27" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.5875" x2="0.3175" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.27" x2="0.3175" y2="0.9525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.127" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<text x="-2.6875" y="2.6145" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-4.0763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM2,5-6,3">
<wire x1="2.735" y1="0.97" x2="2.4175" y2="0.97" width="0.127" layer="21"/>
<wire x1="2.1" y1="0.97" x2="2.4175" y2="0.97" width="0.127" layer="21"/>
<wire x1="2.4175" y1="1.2875" x2="2.4175" y2="0.97" width="0.127" layer="21"/>
<wire x1="2.4175" y1="0.97" x2="2.4175" y2="0.6525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.2" width="0.127" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<text x="-1.6875" y="1.5145" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.876" y="-2.7763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM3,5-8">
<wire x1="1.1" y1="1.1875" x2="0.7825" y2="1.1875" width="0.127" layer="21"/>
<wire x1="0.465" y1="1.1875" x2="0.7825" y2="1.1875" width="0.127" layer="21"/>
<wire x1="0.7825" y1="1.505" x2="0.7825" y2="1.1875" width="0.127" layer="21"/>
<wire x1="0.7825" y1="1.1875" x2="0.7825" y2="0.87" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<pad name="P$1" x="-1.84" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<pad name="P$2" x="1.84" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<text x="-1.6875" y="1.832" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.8113" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM5-10">
<wire x1="1.735" y1="0.87" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.87" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.1875" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<wire x1="1.4175" y1="0.87" x2="1.4175" y2="0.5525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<text x="-1.6875" y="1.197" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.1763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM5-13">
<wire x1="1.735" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.505" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.1875" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="2.1844" shape="octagon" rot="R90"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="2.1844" shape="octagon" rot="R90"/>
<text x="-1.6875" y="1.832" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.8113" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM5-5">
<wire x1="1.905" y1="1.27" x2="1.5875" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.5875" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.5875" y1="1.5875" x2="1.5875" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.5875" y1="1.27" x2="1.5875" y2="0.9525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<text x="-2.6875" y="2.6145" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-4.0763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM5-6,3">
<wire x1="1.635" y1="0.77" x2="1.3175" y2="0.77" width="0.127" layer="21"/>
<wire x1="1" y1="0.77" x2="1.3175" y2="0.77" width="0.127" layer="21"/>
<wire x1="1.3175" y1="1.0875" x2="1.3175" y2="0.77" width="0.127" layer="21"/>
<wire x1="1.3175" y1="0.77" x2="1.3175" y2="0.4525" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.2" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon" rot="R90"/>
<text x="-1.6875" y="1.197" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.1763" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM7,5-16">
<wire x1="1.735" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.505" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.1875" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<circle x="0" y="0" radius="8" width="0.127" layer="21"/>
<pad name="P$2" x="3.81" y="0" drill="0.8" diameter="2.54" shape="octagon"/>
<pad name="P$1" x="-3.81" y="0" drill="0.8" diameter="2.54" shape="octagon"/>
<text x="-1.6875" y="1.832" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.8113" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM7,5-18">
<wire x1="1.735" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.505" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.1875" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<circle x="0" y="0" radius="9" width="0.127" layer="21"/>
<pad name="P$2" x="3.81" y="0" drill="0.8" diameter="2.54" shape="octagon"/>
<pad name="P$1" x="-3.81" y="0" drill="0.8" diameter="2.54" shape="octagon"/>
<text x="-1.6875" y="1.832" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.8113" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ELEKTROLITICKY_RM10-22">
<wire x1="1.735" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.1875" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.505" x2="1.4175" y2="1.1875" width="0.127" layer="21"/>
<wire x1="1.4175" y1="1.1875" x2="1.4175" y2="0.87" width="0.127" layer="21"/>
<circle x="0" y="0" radius="11" width="0.127" layer="21"/>
<pad name="P$2" x="5.08" y="0" drill="0.8" diameter="3" shape="octagon"/>
<pad name="P$1" x="-5.08" y="0" drill="0.8" diameter="3" shape="octagon"/>
<text x="-1.6875" y="1.832" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.676" y="-2.8113" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="KERAMICKY_RM2">
<wire x1="-1.5875" y1="-0.9525" x2="-1.5875" y2="0.9525" width="0.127" layer="21" curve="-180"/>
<wire x1="-1.5875" y1="0.9525" x2="1.5875" y2="0.9525" width="0.127" layer="21" curve="-32.779081"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.9525" width="0.127" layer="21" curve="-180"/>
<wire x1="1.5875" y1="-0.9525" x2="-1.5875" y2="-0.9525" width="0.127" layer="21" curve="-32.779081"/>
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.9304" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.9304" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.4638" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="KERAMICKY_RM5">
<wire x1="-1.5875" y1="-0.9525" x2="-1.5875" y2="0.9525" width="0.127" layer="21" curve="-180"/>
<wire x1="-1.5875" y1="0.9525" x2="1.5875" y2="0.9525" width="0.127" layer="21" curve="-32.779081"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.9525" width="0.127" layer="21" curve="-180"/>
<wire x1="1.5875" y1="-0.9525" x2="-1.5875" y2="-0.9525" width="0.127" layer="21" curve="-32.779081"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.9304" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SMD0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.3175" y="-0.3175" size="0.6096" layer="51">C</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="SMD0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.3175" y="-0.3175" size="0.6096" layer="51">C</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="SMD1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.3175" y="-0.3175" size="0.6096" layer="51">C</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="KONDENZATOR+">
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="KONDENZATOR">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ELEKTROLITICKY" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="KONDENZATOR+" x="0" y="0"/>
</gates>
<devices>
<device name="RM1,5-4" package="ELEKTROLITICKY_RM1,5-4">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM2,5-5" package="ELEKTROLITICKY_RM2,5-5">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM2,5-6,3" package="ELEKTROLITICKY_RM2,5-6,3">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM3,5-8" package="ELEKTROLITICKY_RM3,5-8">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM5-10" package="ELEKTROLITICKY_RM5-10">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM5-13" package="ELEKTROLITICKY_RM5-13">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM5-5" package="ELEKTROLITICKY_RM5-5">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM5-6,3" package="ELEKTROLITICKY_RM5-6,3">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM7,5-16" package="ELEKTROLITICKY_RM7,5-16">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM7,5-18" package="ELEKTROLITICKY_RM7,5-18">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM10-22" package="ELEKTROLITICKY_RM10-22">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KERAMICKY" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="KONDENZATOR" x="0" y="0"/>
</gates>
<devices>
<device name="RM2" package="KERAMICKY_RM2">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RM5" package="KERAMICKY_RM5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD0603" package="SMD0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD0805" package="SMD0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD1206" package="SMD1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="#pitrik1" deviceset="ARK300-2" device=""/>
<part name="X3" library="#pitrik1" deviceset="ARK300-3" device=""/>
<part name="X4" library="#pitrik1" deviceset="ARK300-3" device=""/>
<part name="X5" library="#pitrik1" deviceset="ARK300-3" device=""/>
<part name="X6" library="#pitrik1" deviceset="ARK300-3" device=""/>
<part name="D1" library="#_GM99" deviceset="1N4007" device="" value="4 x 1N4007"/>
<part name="D2" library="#_GM99" deviceset="1N4007" device=""/>
<part name="D3" library="#_GM99" deviceset="1N4007" device=""/>
<part name="D4" library="#_GM99" deviceset="1N4007" device=""/>
<part name="GND1" library="#PaJa_16" deviceset="GND" device=""/>
<part name="GND2" library="#PaJa_16" deviceset="GND" device=""/>
<part name="U1" library="#_GM99" deviceset="7805" device="" value="7805"/>
<part name="C1" library="#Kondenzatory" deviceset="ELEKTROLITICKY" device="RM2,5-6,3" value="470u"/>
<part name="C2" library="#Kondenzatory" deviceset="ELEKTROLITICKY" device="RM2,5-5" value="100u"/>
<part name="C3" library="#Kondenzatory" deviceset="KERAMICKY" device="RM5" value="M1"/>
<part name="GND3" library="#PaJa_16" deviceset="GND" device=""/>
<part name="GND4" library="#PaJa_16" deviceset="GND" device=""/>
<part name="GND5" library="#PaJa_16" deviceset="GND" device=""/>
<part name="GND6" library="#PaJa_16" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="G$1" x="15.24" y="45.72" rot="R180"/>
<instance part="X3" gate="G$1" x="109.22" y="63.5"/>
<instance part="X4" gate="G$1" x="109.22" y="50.8"/>
<instance part="X5" gate="G$1" x="109.22" y="38.1"/>
<instance part="X6" gate="G$1" x="109.22" y="25.4"/>
<instance part="D1" gate="G$1" x="33.02" y="60.96"/>
<instance part="D2" gate="G$1" x="33.02" y="53.34" rot="R180"/>
<instance part="D3" gate="G$1" x="33.02" y="43.18"/>
<instance part="D4" gate="G$1" x="33.02" y="35.56" rot="R180"/>
<instance part="GND1" gate="ZEM" x="96.52" y="15.24"/>
<instance part="GND2" gate="ZEM" x="40.64" y="25.4"/>
<instance part="U1" gate="G$13" x="66.04" y="60.96"/>
<instance part="C1" gate="G$1" x="48.26" y="50.8"/>
<instance part="C2" gate="G$1" x="81.28" y="50.8"/>
<instance part="C3" gate="G$1" x="88.9" y="50.8"/>
<instance part="GND3" gate="ZEM" x="48.26" y="40.64"/>
<instance part="GND4" gate="ZEM" x="66.04" y="40.64"/>
<instance part="GND5" gate="ZEM" x="81.28" y="40.64"/>
<instance part="GND6" gate="ZEM" x="88.9" y="40.64"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="K"/>
<wire x1="25.4" y1="43.18" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="27.94" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<junction x="25.4" y="43.18"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="48.26" x2="25.4" y2="48.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="48.26" x2="25.4" y2="53.34" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="25.4" y1="53.34" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="27.94" y1="53.34" x2="25.4" y2="53.34" width="0.1524" layer="91"/>
<junction x="25.4" y="53.34"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="38.1" y1="53.34" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
<wire x1="40.64" y1="53.34" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND2" gate="ZEM" pin="GND"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="40.64" y1="35.56" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="38.1" y1="35.56" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<junction x="40.64" y="35.56"/>
</segment>
<segment>
<pinref part="X5" gate="G$1" pin="1"/>
<pinref part="GND1" gate="ZEM" pin="GND"/>
<wire x1="101.6" y1="40.64" x2="96.52" y2="40.64" width="0.1524" layer="91"/>
<wire x1="96.52" y1="40.64" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<wire x1="96.52" y1="38.1" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="96.52" y1="35.56" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<wire x1="96.52" y1="27.94" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<wire x1="96.52" y1="22.86" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<pinref part="X6" gate="G$1" pin="3"/>
<wire x1="101.6" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="22.86" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="X6" gate="G$1" pin="2"/>
<wire x1="96.52" y1="25.4" x2="101.6" y2="25.4" width="0.1524" layer="91"/>
<pinref part="X6" gate="G$1" pin="1"/>
<wire x1="101.6" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<pinref part="X5" gate="G$1" pin="3"/>
<wire x1="101.6" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<junction x="96.52" y="22.86"/>
<junction x="96.52" y="25.4"/>
<junction x="96.52" y="27.94"/>
<junction x="96.52" y="35.56"/>
<junction x="96.52" y="38.1"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="48.26" y1="45.72" x2="48.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND3" gate="ZEM" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$13" pin="GND"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND4" gate="ZEM" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="81.28" y1="45.72" x2="81.28" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND5" gate="ZEM" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND6" gate="ZEM" pin="GND"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$13" pin="VI"/>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="55.88" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="48.26" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="43.18" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="38.1" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="43.18" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<junction x="43.18" y="60.96"/>
<junction x="48.26" y="60.96"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$13" pin="VO"/>
<wire x1="76.2" y1="60.96" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<wire x1="88.9" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<wire x1="96.52" y1="63.5" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="1"/>
<wire x1="96.52" y1="66.04" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="2"/>
<wire x1="101.6" y1="63.5" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="3"/>
<wire x1="96.52" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="X4" gate="G$1" pin="3"/>
<wire x1="96.52" y1="53.34" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="48.26" width="0.1524" layer="91"/>
<wire x1="96.52" y1="48.26" x2="101.6" y2="48.26" width="0.1524" layer="91"/>
<pinref part="X4" gate="G$1" pin="2"/>
<wire x1="101.6" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="X4" gate="G$1" pin="1"/>
<wire x1="96.52" y1="53.34" x2="101.6" y2="53.34" width="0.1524" layer="91"/>
<junction x="96.52" y="53.34"/>
<junction x="96.52" y="50.8"/>
<junction x="96.52" y="60.96"/>
<junction x="96.52" y="63.5"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="81.28" y1="53.34" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="88.9" y1="53.34" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<junction x="81.28" y="60.96"/>
<junction x="88.9" y="60.96"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
