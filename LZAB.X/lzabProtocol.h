/* 
 * File:   lzabProtocol.h
 * Author: Martin Pitrik
 *
 * Protocol values.
 *
 * Created on 19. duben 2017, 20:32
 */

#ifndef LZABPROTOCOL_H
#define	LZABPROTOCOL_H

#ifdef	__cplusplus
extern "C" {
#endif

#define STUJ 0x01
#define VOLNO 0x02
#define VOLNO_40 0x03
#define VOLNO_40_40 0x04
#define VOLNO_OC_40 0x05
#define VYSTRAHA 0x06
#define VYSTRAHA_40 0x07
#define VS_O 0x08
#define VS_V 0x09
#define PN 0x0A

#define V_ODB 0x10
#define V_PR 0x11

#define ERROR 0xFF

#ifdef	__cplusplus
}
#endif

#endif	/* LZABPROTOCOL_H */

