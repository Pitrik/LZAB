/* 
 * File:   main.c
 * Author: Martin Pitrik
 *
 * Created on 29. leden 2017, 9:57
 */

#include <stdio.h>
#include <stdlib.h>
#include <pic16f628.h>
#include <xc.h>

#include "type.h"
#include "core.h"
#include "lzabProtocol.h"

// PIC16F628A Configuration Bit Settings
#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = ON       // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is MCLR)
#pragma config BOREN = ON       // Brown-out Detect Enable bit (BOD enabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define DEV_ADDR 1  

boolean PN1 = FALSE;
boolean PN2 = FALSE;

void delay(unsigned int base, unsigned int count)
{
    unsigned int i;
    unsigned int j;
    for (j = 0; j < count; ++j)
    {
        for (i = 0; i < base; ++i);
    }
}

void init()
{
    coreInit();
}

unsigned char readState(unsigned char reg)
{
    //
    switch (reg)
    {
        case 1:
            return PORTBbits.RB0;
        case 2:
            return PORTBbits.RB4;
        default:
            return 2;
    }
    //
}

//writeInstructionRegs[decodeInstruction.regAddress] = decodeInstruction.recData;

boolean initModulLogic(void)
{
    //
    if (DEV_ADDR == 1 || DEV_ADDR == 3)
    {
        unsigned int signalState = writeInstructionRegs[1];
        unsigned int switchState = writeInstructionRegs[2];
        //unsigned int agreementState = writeInstructionRegs[2];
        //not implemented, communication with others systems

        switch(signalState)
        {
            case UNSET:
            case STUJ:
                PORTAbits.RA7 = 1;
                PORTAbits.RA1 = 0;
                PORTAbits.RA2 = 0;
                PN1 = FALSE;
                break;
            case VOLNO:
                PORTAbits.RA1 = 1;
                PORTAbits.RA7 = 0;
                PORTAbits.RA2 = 0;
                PN1 = FALSE;
                break;
            case VYSTRAHA:
                PORTAbits.RA2 = 1;
                PORTAbits.RA7 = 0;
                PORTAbits.RA1 = 0;
                PN1 = FALSE;
                break;
            case PN:
                PORTAbits.RA7 = 1;
                PORTAbits.RA1 = 0;
                PORTAbits.RA2 = 0;
                PN1 = TRUE;
            default:
                return ERROR;
        }

        switch(switchState)
        {
            case UNSET:
                break;
            case V_ODB:
                PORTAbits.RA6 = 1;
                PORTAbits.RA0 = 0;
                break;
            case V_PR:
                PORTAbits.RA0 = 1;
                PORTAbits.RA6 = 0;
                break;
            default:
                return ERROR;
        }

        //switch(agreementState) 
    }
    else if (DEV_ADDR == 2)
    {
        unsigned int signalState1 = writeInstructionRegs[1];
        unsigned int signalState2 = writeInstructionRegs[2];
        unsigned int signalState3 = writeInstructionRegs[3];
        unsigned int signalState4 = writeInstructionRegs[4];
        
        unsigned int signal1 = STUJ;
        unsigned int signal2 = STUJ;

        if (signalState1 != STUJ && signalState2 == STUJ)
        {
            signal1 = signalState1;
        }
        else if (signalState1 == STUJ && signalState2 != STUJ)
        {
            signal1 = signalState2;
        }

        if (signalState3 != STUJ && signalState4 == STUJ)
        {
            signal2 = signalState3;
        }
        else if (signalState3 == STUJ && signalState4 != STUJ)
        {
            signal2 = signalState4;
        }

        switch(signal1)
        {
            case UNSET:
            case STUJ:
                PORTAbits.RA7 = 1;
                PORTAbits.RA1 = 0;
                PN1 = FALSE;
                break;
            case VOLNO:
                PORTAbits.RA1 = 1;
                PORTAbits.RA7 = 0;
                PN1 = FALSE;
                break;
            case PN:
                PORTAbits.RA7 = 1;
                PORTAbits.RA1 = 0;
                PN1 = TRUE;
            default:
                return ERROR;
        }

        switch(signal2)
        {
            case UNSET:
            case STUJ:
                PORTAbits.RA3 = 1;
                PORTAbits.RA6 = 0;
                PN2 = FALSE;
                break;
            case VOLNO:
                PORTAbits.RA6 = 1;
                PORTAbits.RA3 = 0;
                PN2 = FALSE;
                break;
            case PN:
                PORTAbits.RA3 = 1;
                PORTAbits.RA6 = 0;
                PN2 = TRUE;
            default:
                return ERROR;
        }
    }

    return TRUE;
    //
}

void modulLogic(void)
{
    if (DEV_ADDR == 1 || DEV_ADDR == 3)
    {
        if (PN1 == TRUE)
        {
            PORTAbits.RA3 = 1;
            delay(30000, 1);
            PORTAbits.RA3 = 0;
            delay(30000, 1);
        }
        else
        {
            PORTAbits.RA3 = 0;
        }
    }
    else if (DEV_ADDR == 2)
    {
        if (PN1 == TRUE)
        {
            PORTAbits.RA2 = 1;
            delay(30000, 1);
            PORTAbits.RA2 = 0;
            delay(30000, 1);
        }
        else
        {
            PORTAbits.RA2 = 0;
        }

        if (PN2 == TRUE)
        {
            PORTAbits.RA0 = 1;
            delay(30000, 1);
            PORTAbits.RA0 = 0;
            delay(30000, 1);
        }
        else
        {
            PORTAbits.RA0 = 0;
        }
    }
}

/*
 * Main method
 */
int main(void)
{   
    init();

    while(TRUE)
    {
        decodeInstructionAndSetRegisters(DEV_ADDR);
        modulLogic();
    }

    return (EXIT_SUCCESS);
}

void interrupt MCU_int(void)
{
    __interrupt__();
}

