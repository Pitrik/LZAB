/* 
 * File:   type.h
 * Author: Martin Pitrik
 *
 * Created on 19. �nor 2017, 11:06
 */

#ifndef TYPE_H
#define	TYPE_H

#ifdef	__cplusplus
extern "C" {
#endif

#define TRUE    1
#define FALSE   0

typedef unsigned char boolean;

#ifdef	__cplusplus
}
#endif

#endif	/* TYPE_H */