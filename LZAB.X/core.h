/* 
 * File:   modbus.h
 * Author: Martin Pitrik
 *
 * Modul realizes communication with computer and decode receiver and
 * transitter data.
 *
 * Created on 19. �nor 2017, 11:07
 */

#ifndef CORE_H
#define	CORE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <pic16f628.h>
#include "type.h"

//:
//1, 2 address
//3, 4 function
//5, 6, 7, 8 address subsystem
//9, 10, 11, 12 data
//13, 14 LRC
//15, 16 EOL

//message offsets
#define ADDR_OFFSET 1
#define FUNC_OFFSET 3
#define REG_OFFSET 5
#define DATA_OFFSET 9

//function codes
#define UNKNOWN 0
#define READ_I_REG 4
#define WRITE_S_REG 6
#define IS_NOT_FOR_DEV 255

#define READ_REGS_NUMBER 8      //number of read registers
#define WRITE_REGS_NUMBER 8     //number of write registers

//EOL constants
#define LF  0x0A
#define CR  0x0D

#define BUFFER_SIZE 17          //UART buffer size

#define UNSET 0

//Instruction type
typedef struct
{
    int function;               //function code
    int regAddress;             //register address
    int recData;                //data
}
instruction;

//Global variables
//Write registers
unsigned int writeInstructionRegs[WRITE_REGS_NUMBER];

//Data buffer
unsigned char data[BUFFER_SIZE];

//Valid data flag
boolean isValid;

//Write char to serial port
void writeChar(unsigned char data);

//Write string to serial port
void writeString(unsigned char *data, unsigned char lenght);

//Write 8bits register
void SendByte(unsigned char address, unsigned char data);

//Write ACK
void SendAck(void);

//Write error
void SendError(void);

//Decode instruction for this device
instruction decodeInstructionForDevice(unsigned char deviceAddress);

//Decode number to HEX ASCII char.
unsigned char decodeNumberToHexChar(unsigned int number);

//Decode HEX ASCII char to DEC number.
int decodeHexCharToNumber(char data);

//Decode two bytes
int decodeTwoChars(char *data, unsigned char start);

//Decode four bytes
int decodeFourChars(char *data, unsigned char start);

//Decode instruction and set register
void decodeInstructionAndSetRegisters(unsigned char deviceAddress);

//UART interrupt
void __interrupt__();

//Extern function for read register state according to register address
extern unsigned char readState(unsigned char reg);

//Extern function for init modul
extern boolean initModulLogic(void);

void coreInit(void);

#ifdef	__cplusplus
}
#endif

#endif	/* CORE_H */
