/* 
 * File:   modbus.c
 * Author: Martin
 *
 * Created on 19. �nor 2017, 11:09
 */

#include "core.h"

void writeChar(unsigned char data)
{
    PORTBbits.RB3 = 1;
    TXREG = data;
    while (!TRMT);
    PORTBbits.RB3 = 0;
}

void writeString(unsigned char *data, unsigned char lenght)
{
    unsigned char i;
    for (i = 0; i < lenght; ++i)
    {
        writeChar(data[i]);
    }
}

void SendByte(unsigned char address, unsigned char data)
{
    unsigned char addr1 = (unsigned char)(address / (float)16);
    unsigned char addr2 = address % 16;
    unsigned char val1 = (unsigned char)(data / (float)16);
    unsigned char val2 = data % 16;
    unsigned char lcr = (unsigned char)(~(address + 0x04 + 0x02 + val1 + val2)) + 1;
    unsigned char val3 = (unsigned char)(lcr / (float)16);
    unsigned char val4 = lcr % 16;

    writeChar(':');         //start message
    writeChar(decodeNumberToHexChar(addr1));
    writeChar(decodeNumberToHexChar(addr2));
    writeString((unsigned char*)"04", 2);   //function code
    writeString((unsigned char*)"02", 2);   //block size
    writeString((unsigned char*)"00", 2);   //unused bytes
    writeChar(decodeNumberToHexChar(val1)); //value H
    writeChar(decodeNumberToHexChar(val2)); //value L
    writeChar(decodeNumberToHexChar(val3)); //lcr H
    writeChar(decodeNumberToHexChar(val4)); //lcr L
    writeChar(CR);  //end of message
    writeChar(LF);
}

void SendAck()
{
    writeString(data, BUFFER_SIZE);     //send message back
}

void SendError()
{
    writeChar(':');         //start message
    writeString((unsigned char*)"81", 2);   //function code
    writeString((unsigned char*)"03", 2);   //error code
    writeChar(CR);  //end of message
    writeChar(LF);
}

instruction decodeInstructionForDevice(unsigned char deviceAddress)
{
    instruction decodeInstruction;

    //decode address
    int address = decodeTwoChars(data, ADDR_OFFSET);
    if (address == 0 || address == deviceAddress)    //broadcast or device address
    {       
        decodeInstruction.function = decodeTwoChars(data, FUNC_OFFSET);
        decodeInstruction.regAddress = decodeFourChars(data, REG_OFFSET);
        decodeInstruction.recData = decodeFourChars(data, DATA_OFFSET);
    }
    else
    {
        decodeInstruction.function = IS_NOT_FOR_DEV;
    }

    return (decodeInstruction);
}

unsigned char decodeNumberToHexChar(unsigned int number)
{
    if (number <= 9)
    {
        return number + '0';
    }

    switch (number)
    {
        case 10:
            return 'A';
        case 11:
            return 'B';
        case 12:
            return 'C';
        case 13:
            return 'D';
        case 14:
            return 'E';
        case 15:
            return 'F';
        default:
            return 'N';
    }
}

int decodeHexCharToNumber(char data)
{
    if (data >= '0' && data <= '9')
    {
        return data - '0';
    }

    switch (data)
    {
        case 'A':
            return 10;
        case 'B':
            return 11;
        case 'C':
            return 12;
        case 'D':
            return 13;
        case 'E':
            return 14;
        case 'F':
            return 15;
        default:
            return 'X';
    }
}

int decodeTwoChars(char *data, unsigned char start)
{
    unsigned char result = 0;
    unsigned char char1 = data[start];
    unsigned char char2 = data[start + 1];

    unsigned int val1 = decodeHexCharToNumber(char1);
    unsigned int val2 = decodeHexCharToNumber(char2);

    if (val1 == -1 || val2 == -1)
    {
        return -1;
    }

    result = val1 * 16 + val2;

    return result;
}

int decodeFourChars(char *data, unsigned char start)
{
    unsigned char result = 0;

    unsigned int val1 = decodeTwoChars(data, start);   
    unsigned int val2 = decodeTwoChars(data, start + 2);  

    if (val1 == -1 || val2 == -1)
    {
        return -1;
    }

    result = val1 * 256 + val2;

    return result;
}

void decodeInstructionAndSetRegisters(unsigned char deviceAddress)
{
    if (isValid == TRUE)
    {
        isValid = FALSE;
        instruction decodeInstruction = decodeInstructionForDevice(deviceAddress);
        if (decodeInstruction.function == IS_NOT_FOR_DEV)
        {
            return;
        }

        if (decodeInstruction.function == READ_I_REG)
        {          
            if (decodeInstruction.regAddress < READ_REGS_NUMBER)
            {
                unsigned char state = readState(decodeInstruction.regAddress);
                SendByte(deviceAddress, state);
            }
            else
            {
                SendError();
            }
        }
        else if (decodeInstruction.function == WRITE_S_REG)
        {
            if (decodeInstruction.regAddress < WRITE_REGS_NUMBER)
            {
                writeInstructionRegs[decodeInstruction.regAddress] = decodeInstruction.recData;
                boolean isOk = initModulLogic();
                if (isOk == TRUE)
                {
                    SendAck();
                }
                else
                {
                    SendError();
                }
            }
            else
            {
                SendError();
            }
        }
        else
        {
            SendError();
        }
    }
}

void __interrupt__()
{
    static unsigned char index = 0;

    RCIF = 0;   //clear receiver bit
    data[index++] = RCREG;    //read
    
    if (data[index - 1] == LF && data[index - 2] == CR)     //message end
    {       
        if (data[0] == ':')
        {           
            isValid = TRUE;
            index = 0;
        }
        else        //message corrupted, clear buffer and read again
        {
            index = 0;
        }
    }

    if (index == BUFFER_SIZE)   //buffer overflow
    {
        index = 0;
    }
}

void coreInit(void)
{
    PORTA = 0x00;
    PORTB = 0x00;
    //Port direction, 0 is output, 1 is input
    TRISA = 0x00;       //output port
    TRISB = 0xF3;       //RB2, RB3 outputs, others inputs
    CMCON = 0x07;       //turn off comparators and enable pins to I/O function
    nRBPU = 0;          //enable pull-up internal rezistors for each input ports
    SPEN = 1;           //enable serial port
    RCSTA = 0x90;       //select 8bit mode, enable continous receive
    TXSTA = 0x24;       //select 8bit mode, transmit enable, async mode, BRGH = 1
    SPBRG = 0x19;       //baud rate 9600 / fosc = 4 MHz / BRGH = 1

    PORTBbits.RB3 = 0;  //read uart enabled

    //enable interrupts
    GIE = 1;
    RCIE = 1;
    PEIE = 1;

    isValid = FALSE;

    for (unsigned char i = 0; i < WRITE_REGS_NUMBER; ++i)
    {
        writeInstructionRegs[i] = 0;
    }
}