﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LRCTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var address = Convert.ToInt32(txtAddress.Text, 16);
            var code = Convert.ToInt32(txtCode.Text, 16);
            var data = txtData.Text;

            var dataSplit = (from Match m in Regex.Matches(data, "..") select m.Value).Select(v => Convert.ToInt32(v, 16)).ToArray();
            var total = address + code + dataSplit.Sum();

            var lrc = (ushort)(~total) + 1;

            txtLrc.Text = $@"{lrc:X}".Substring(2);
        }
    }
}
